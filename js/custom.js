jQuery( function($) {
    // Hide Navbar on scroll down
    var didScroll;
    var lastScrollTop = 0;
    var delta = 5;

    var navbarHeight = $('nav').outerHeight();

    $(window).scroll(function(event){
        didScroll = true;
    });

    setInterval(function() {
        if (didScroll) {
            didScroll = false;
        }
    }, 250);

    // function hasScrolled() {
    //     var st = $(this).scrollTop();

    //     // Make sure they scroll more than delta
    //     if(Math.abs(lastScrollTop - st) <= delta)
    //         return;

    //     // If they scrolled down and are past the navbar, add class .nav-up.
    //     // This is necessary so you never see what is "behind" the navbar.
    //     if (st > lastScrollTop && st > navbarHeight){
    //         // Scroll Down
    //         $('nav').css({'top':-navbarHeight});
    //     } else {
    //         // Scroll Up
    //         if(st + $(window).height() < $(document).height()) {
    //             $('nav').css({'top':'0'});
    //         }
    //     }

    //     lastScrollTop = st;
    // }

    // Change Navbar style when page is scrolled
    $(window).scroll(function()
    {
        if ($(this).scrollTop() > 100)
        {
            $('nav').addClass('navbar-scrolled');
        }
        else {
            $('nav').removeClass('navbar-scrolled');
        }
    });


    var Offcanvas = document.getElementById('offcanvasNav')
    Offcanvas.addEventListener('show.bs.offcanvas', function () {
        $('body').addClass('offcanvas-open');
    })
    Offcanvas.addEventListener('hide.bs.offcanvas', function () {
        $('body').removeClass('offcanvas-open');
    })


    // Counter
    var $findme = $('#numbers');
    var exec = false;
    function Scrolled() {
    $findme.each(function() {
        var $section = $(this),
        findmeOffset = $section.offset(),
        findmeTop = findmeOffset.top,
        findmeBottom = $section.height() + findmeTop,
        scrollTop = $(document).scrollTop(),
        visibleBottom = window.innerHeight,
        prevVisible = $section.prop('_visible');

        if ((findmeTop > scrollTop + visibleBottom) ||
        findmeBottom < scrollTop) {
        visible = false;
        } else visible = true;

        if (!prevVisible && visible) {
            if(!exec){
                $('.counter').each(function() {
            $(this).prop('Counter', 0).animate({
                Counter: $(this).text()
            }, {
                duration: 3000,

                step: function(now) {
                $(this).text(Math.ceil(now));
                exec = true;
                }
            });
            });
        }
        }
        $section.prop('_visible', visible);
    });
    }
    function Setup() {
    var $top = $('#top'),
        $bottom = $('#bottom');

    $top.height(500);
    $bottom.height(500);

    $(window).scroll(function() {
        Scrolled();
    });
    }
    $(document).ready(function() {
    Setup();
    });
});



window.addEventListener('DOMContentLoaded', (e) => {
    var gradient = new Gradient;
    gradient.initGradient("#gradient-canvas");
});

window.addEventListener('DOMContentLoaded', (e) => {
    var gradientOffCanvas = new Gradient;
    gradientOffCanvas.initGradient("#gradient-canvas-offcanvas");
});

AOS.init();