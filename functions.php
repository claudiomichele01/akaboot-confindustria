<?php

add_theme_support( 'title-tag' );

@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );

function processButtonHref($type_link){
    $type_link = (array) $type_link;
    $properties = array_keys($type_link);
    $vendor = array_filter($properties, fn($p) => preg_match('/_link_vendor/', $p));
    $prefix = str_replace('_link_vendor', '', reset($vendor));

	switch( $type_link["{$prefix}_link_type"] ) {

		case "Prodotti":
            return get_permalink( $type_link["{$prefix}_link_product"] );

		case "Pagine":
            return get_permalink( $type_link["{$prefix}_link_page"] );

		case "Venditori":
            return get_term_link( $type_link["{$prefix}_link_vendor"] );

		case "Categorie":
            return get_term_link( $type_link["{$prefix}_link_cat"] );

		case "Url personalizzato":
            return $type_link["{$prefix}_link_url"];

		default:
		    return "";

	}
}




add_action( 'phpmailer_init', 'set_phpmailer_details', 10, 1 );
function set_phpmailer_details( $phpmailer )
{
    $phpmailer->isSMTP();
    $phpmailer->Host = 'mail.divertidenti.it';
    $phpmailer->SMTPAuth = true;
    $phpmailer->Port = '587';
    $phpmailer->Username = 'progettoscuola@divertidenti.it';
    $phpmailer->Password = 'MrWhiteDvRtDnT21!';
// $phpmailer->SMTPSecure = 'tsl'; // ssl or tls
    $phpmailer->SMTPAutoTLS = false;
    $phpmailer->SMTPOptions = array(
        'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
        )
    );

    error_log('SMTP setup done');
}

add_action('wp_mail_failed', 'log_mailer_errors', 10, 1);
function log_mailer_errors( $wp_error ){
    //error_log($wp_error->get_error_code());
    error_log($wp_error->get_error_message());
    //error_log(print_r( $wp_error, true ));
    
}


add_filter( 'wp_mail_from', 'wpb_sender_email' );
add_filter( 'wp_mail_from_name', 'wpb_sender_name' );
function wpb_sender_email( $original_email_address )
{
    return FROM_EMAIL_ADDRESS;
}
function wpb_sender_name( $original_email_from )
{
    return FROM_EMAIL_NAME;
}

add_filter("retrieve_password_message", "mapp_custom_password_reset", 99, 4);

function mapp_custom_password_reset($message, $key, $user_login, $user_data )    {

    $message = "Ciao, abbiamo ricevuto la tua richiesta di recupero password.
        
Se non hai effettuato tu la richiesta ignora questo messaggio.
        
Per reimpostare la password clicca sul link qua sotto:
        
" . route("edit_password" , ["rp_key" => $key , "user_row_url" => rawurlencode($user_login)]) . "\r\n"  /*'<' . network_site_url("wp-login.php?action=rp&key=$key&login=" . rawurlencode($user_login), 'login') . ">\r\n" */. "
        
Se continuare a riscontrare problemi di accesso contattateci all'indirizzo: progettoscuola@divertidenti.it";


    return $message;

}