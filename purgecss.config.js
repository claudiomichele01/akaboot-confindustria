module.exports = {
    content: ['views/**/*.blade.php'],
    css: ['css-compiled/*.css'],
    safelist: {
        greedy: [/p-2$/, /modal$/, /ms-2$/, /navbar$/, /offcanvas$/],
    },
    rejected: true,
    output: ['css']
}