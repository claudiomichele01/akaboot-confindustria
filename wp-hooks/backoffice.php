<?php

/* //codice lasciato come esempio
// Adds custom column
add_filter( 'manage_shop_order_posts_columns', function($columns) {

    $columns['per-chiara'] = __( 'Per Chiara', 'splitlang' );
    return $columns;

}, 99, 1 );

add_action( 'manage_shop_order_posts_custom_column' , function( $column, $post_id ) {
    switch ( $column ) {

        case 'per-chiara':
            
            $order = wc_get_order( $post_id );

            if ( !$order )
                return;

            $items = $order->get_items();

            echo "<ul style=\"list-style: outside\">";
            array_map( function( $item ) {

                echo "<li style=\"font-size: 80%\">" . $item->get_quantity() . " x " . $item->get_name() . " - " . $item->get_variation_id() . "</li>\n";

            }, $items);
            echo "</ul>";


            break;
    }
}, 99, 2 );
*/