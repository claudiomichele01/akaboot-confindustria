<?php

//Previene doppio caricamento
function akaboot_add_scripts() {

    $extension = "min.css";
    $subpath = "css-compiled";
    $cssList = [
        "core",
        "tables",
        "forms",
        "buttons",
        "transitions",
        "dropdown",
        "button-group",
        "nav",
        "navbar",
        "card",
        "accordion",
        "breadcrumb",
        "pagination",
        "badge",
        "alert",
        "progress",
        "list-group",
        "close",
        "toasts",
        "modal",
        "tooltip",
        "popover",
        "carousel",
        "spinners",
        "offcanvas",
        "helpers",
        "api",

        "icons",
        "fonts",
        "custom"
    ];

    foreach($cssList as $css) {
        $id = "akaboot-" . $css;
        wp_register_style( $id, get_stylesheet_directory_uri() . "/{$subpath}/{$css}.{$extension}", [], '1.2' );
        wp_enqueue_style( $id );
    }

    wp_register_script( 'bootstrap-js', get_stylesheet_directory_uri() . '/js/bootstrap/bootstrap.bundle.min.js', array('jquery'), '1.0', true );
    wp_enqueue_script('bootstrap-js');

    wp_register_script( 'aos-js', get_stylesheet_directory_uri() . '/js/aos.js', array('jquery'), '1.0', true );
    wp_enqueue_script('aos-js');

    wp_register_script( 'hero-gradient-js', get_stylesheet_directory_uri() . '/js/hero-gradient.js', array('jquery'), '1.0', true );
    wp_enqueue_script('hero-gradient-js');

    wp_register_script( 'akaboot-js', get_stylesheet_directory_uri() . '/js/custom.js', array('jquery'), '1.0', true );
    wp_enqueue_script('akaboot-js');
}
add_action( 'wp_enqueue_scripts', 'akaboot_add_scripts' );

function my_login_stylesheet() {
    wp_enqueue_script( 'custom-login', get_stylesheet_directory_uri() . '/wp-custom-js/custom.js' );
}
add_action( 'login_enqueue_scripts', 'my_login_stylesheet' );

//Adds posts to homepage
add_filter( 'akaboot_2blade_page_extra', function( $extra, $page_template ) {

    if( $page_template == "home" ) {
        array_push( $extra, "posts" );
    }

    return $extra;

}, 1, 2);

