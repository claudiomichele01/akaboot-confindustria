<?php
add_filter('akaboot/controller/enrich', 'akaueb_add_acf_details_to_content_creators', 10, 2);
function akaueb_add_acf_details_to_content_creators($parsed, $controller) {
    if(isset($parsed->post_type) && $parsed->post_type == 'content_creators') {
        $acf = get_fields($parsed->ID);
        $parsed->acf = $controller->processData($acf);
    }
    return $parsed;
}
