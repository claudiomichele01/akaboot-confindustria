<?php namespace App\Http;

use Akaboot\HttpKernelBase;

class Kernel extends HttpKernelBase {

    protected $routeMiddleware = [
    ];

    protected $ajaxRoutings = [
    ];

}