<?php

namespace App\Http\Controllers;

use Akaboot\ControllerBase;
use Boot;

class AccessController extends ControllerBase
{

    public function index() {
        return view('pages.access', [
            "wp_title" => "Docenti - Accedi",
            "alternativeTitle" => "Accedi",
        ]);
    }
}