<?php

namespace App\Http\Controllers;

use Akaboot\ControllerBase;
use Boot;

class PersonalAreaController extends ControllerBase
{

    public function index() {
        return view('pages.personalArea', [
            "wp_title" => "Docenti - Area Personale",
            "alternativeTitle" => "Area Personale",
        ]);
    }
}