<?php

namespace App\Http\Controllers;

use Akaboot\ControllerBase;
use Boot;

class RegistrationController extends ControllerBase
{

    public function index() {
        return view('pages.registration', [
            "wp_title" => "Docenti - Registrazione",
            "alternativeTitle" => "Registrazione",
        ]);
    }
}