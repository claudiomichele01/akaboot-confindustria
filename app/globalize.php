<?php

//Formatta una stringa mettendo il simbolo dell'euro e separatori
function formatPrice( $string ) {
    return number_format($string, 2, ',', '.') . " €";
}

//Arrotondamento di Excel
function round_up($value, $places)
{
    $mult = pow(10, abs($places));
    return $places < 0 ? ceil($value / $mult) * $mult : ceil($value * $mult) / $mult;
}