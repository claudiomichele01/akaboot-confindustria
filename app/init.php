<?php namespace App;

use \Boot;
use App\Helpers\SparkNotifications;



if ( ! defined( 'ABSPATH' ) ) exit;

//Definisce le costanti di ambiente
define( 'ASSETS_VERSION', "1.0.72");
define('MAX_PASS_LENGTH', 8);
define('AKA_PLUGIN_PATH', __DIR__ );
define('AKA_GLOBAL_PATH', dirname( __DIR__, 3));

//Personalizzazioni Akaboot School Plugin

//-- permalinks
define('AKA_RESERVED_AREA', 'area-riservata');

define('AKA_TEACHER_PAGE', 'insegnanti');
define('AKA_ADMIN_PAGE', 'amministrazione');

//-- Capabilities

define('AKA_ADMIN_CAP', 'administrator');
define('AKA_TEACH_CAP', 'teacher');

// -- Page fallback
define('UNAUTHORIZED_WEB_VIEW', 'default.404');

// -- configurazione di powerschool

define("POWERSCHOOL_TOKEN" , "36|AZw2n5CIGFhasr6DgTrPUqRQ6r51mLIPYKyzoHCs");
define("POWERSCHOOL_PROJECT_ID" , "124");
define("POWERSCHOOL_FEEDBACK_ENDPOINT", "https://www.powerschool.it/api/instituteregistration");

// -- configurazione email

define("AKABOOT_DISABLE_MAIL", false); //disabilita o meno l'email
define("FROM_EMAIL_ADDRESS" , "progettoscuola@divertidenti.it");
define("FROM_EMAIL_NAME" , "Divertidenti");

// -- Extra

//gradi delle classi accettate
define("AKA_DEFAULT_SCHOOL_GRADE_IDS" , ["1","2"]);

// -- View content Hook react

define('VIEW_CONTENT_HOOK', 'content');

// -- Creazione costanti react
define('REACT_CONST', [

    //nome del progetto
    'projectName' => 'divertidenti',

    //testo della select filtro in area admin
    'mediaFilters' => ['Stato Contenuto Multimediale'],

    //tab dell'area admin
    'adminTabs' => ['Iscritti','Contenuti caricati','Report dati'],



    //filtri di default per la gallery
    'galleryFilters' => [],

    //abilita la ricerca nella gallery
    'gallerySearch' => true,

    //info delle immagini
    'galleryFileInfos' => ['year_section','schoolName','city'],

    //colore della descrizione delle immagini
    'galleryFileInfosTextColor' => 'white',

    //style del container delle immagini
    'galleryFileContainerStyle' => [ "borderRadius" => "0rem" ],

    //numero di immagini per ogni pagina della gallery
    'galleryPagination' => 8,



    //campo privacy da accettare al caricamento
    'privacyRequired' => false,

    //numero massimo di upload per classe
    'maxUploadPerCollection' => 25,

    //campi che vengono visti durante l'upload di un immagine 
    'fileUploadModalInfosDisplay'=>['file'=>true],

    //indica i campi che sono necessari per il caricamento di un'immagine
    'fileUploadModalInfosRequired'=>['file'=>true],

    //numero massimo di caritteri della description
    'uploadDescriptionMaxLength' => 0

]);

require_once('globalize.php');