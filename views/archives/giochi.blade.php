@php
    $giochi = Boot::query( 'giochi' );
	$giochi = array_reverse( $giochi );
@endphp


@extends('layouts.main')

@section('content')

	@component('components.upperContent', [ "args" => [
		"title" => Boot::acf()->options->giochi->band_title ?? tfb(6),
	]])
	@endcomponent

    <section class="py-5">
        <div class="container">
            <div class="row mx-lg-5 mx-2">
                <div class="col-12 text-center">
                    <div class="h1">{!! Boot::acf()->options->giochi->title ?? tfb(6) !!}</div>
					<div>
						{!! Boot::acf()->options->giochi->text ?? tfb(38) !!}
					</div>
                </div>
            </div>
            <div class="row mx-lg-5 mx-2 pt-5">
				@foreach ( $giochi as $gioco)
					@component('components.cards', [ "args" => [
						"layout" => "Cardgame col-12 col-lg-4 col-md-6 mb-4 py-lg-2 px-5 px-lg-0 d-flex",
						"image" => $gioco->thumbnail,
						"title" => $gioco->post_title,
						"text" => $gioco->post_excerpt,
						"button_text" => "Vai",
						"link" => $gioco->permalink,
						"games" => true
					]])
					@endcomponent
				@endforeach
			</div>
        </div>
    </section>

@endsection
