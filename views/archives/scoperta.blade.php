@php
    $scoperte = Boot::query( 'scoperta-dei-denti' );
	$scoperte = array_reverse( $scoperte );
@endphp


@extends('layouts.main')

@section('content')

	@component('components.upperContent', [ "args" => [
		"title" => Boot::acf()->options->scoperta_dei_denti->band_title ?? tfb(7),
	]])
	@endcomponent

    <section class="py-5">
        <div class="container">
            <div class="row mx-lg-5 mx-2">
                <div class="col-12 text-center">
                    <div class="h1">{!! Boot::acf()->options->scoperta_dei_denti->title ?? tfb(17) !!}</div>
					<div>
						{!! Boot::acf()->options->scoperta_dei_denti->text ?? tfb(26) !!}
					</div>
                </div>
            </div>
            <div class="row mx-lg-5 mx-2 pt-5">
				@foreach ( $scoperte as $scoperta )
					@component('components.cards', [ "args" => [
						"layout" => "col-12 col-lg-3 col-md-6 px-lg-4 px-5 mb-4 d-flex",
						"image" => $scoperta->thumbnail,
						"title" => $scoperta->post_title,
						"text" => $scoperta->post_excerpt,
						"button_text" => "Vai",
						"link" => $scoperta->permalink
					]])
					@endcomponent
				@endforeach
			</div>
        </div>
    </section>

@endsection
