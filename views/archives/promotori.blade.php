@php
    $promotori = Boot::query( 'promotori' );
@endphp

@extends('layouts.main')

@section('content')

	@component('components.upperContent', [ "args" => [
		"title" => Boot::acf()->options->promotori->band_title ?? tfb(4),
	]])
	@endcomponent

	<section class="py-5">
		<div class="container">
			<div class="row mx-lg-5 mx-2">
				<div class="col-12 col-lg-6 pe-lg-5">
					<div class="h2 pe-lg-5">{!! Boot::acf()->options->promotori->title ?? tfb(7) !!}</div>
					<div class="py-3">
						{!! Boot::acf()->options->promotori->text ?? tfb(45) !!}
					</div>
					<div class="h6 pe-lg-5">
						{!! Boot::acf()->options->promotori->subtitle ?? tfb(26) !!}
					</div>
					<img src="{!! Boot::acf()->options->promotori->loghi->url ?? ifb() !!}" class="w-75 mt-3" alt="loghi promotori">
				</div>

				<div class="col-12 col-lg-6 mt-lg-0 mt-4">
					<div class="position-relative ratio ratio-4x3">
                        <img src="{!! Boot::acf()->options->promotori->image->url ?? ifb() !!}" class="images" alt="{!! Boot::acf()->options->promotori->image->title ?? tfb(1) !!}">
                    </div>
				</div>
			</div>
		</div>
	</section>
    
    <section class="py-5">
		<div class="container">
			<div class="row mx-lg-5 mx-2">
				<div class="col-12 text-center">
                    <div class="h2 mb-4">{!! Boot::acf()->options->promotori->button_title ?? tfb(3) !!}</div>
					@foreach ($promotori as $promotore)
						<a href="{{$promotore->permalink}}" class="btn btn-primary mx-5 my-2 mt-lg-0 mx-lg-2">{{$promotore->post_title}}</a>
					@endforeach
				</div>
			</div>
		</div>
	</section>
@endsection

