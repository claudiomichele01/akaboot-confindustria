@extends('layouts.main')

@section('content')

    <section class="py-5 gradientBg">
        <div class="container py-5">
            <div class="row mx-lg-5 mx-2">
                <div class="col-lg-3 col-0"></div>
                <div class="col-lg-6 col-12">
                    <div class="card border-0">
                        <div class="card-body p-0">
                            <div class="p-3 px-lg-5">
                                <div class="text-center">
                                    <div class="h2 mt-2">Sei un docente?</div>
                                    <div class="h1">ACCEDI</div>
                                </div>

                                <div class="mx-lg-5 my-4">
                                    Inserisci email e password per accedere alla tua area riservata per scoprire i contenuti e partecipare all’iniziativa.
                                </div>

                                <div class="mx-lg-5 mb-3">
                                    <label for="email" class="small">Scrivi la tua email</label>
                                    <input type="text" class="form-control" id="name" placeholder="">
                                </div>
                                <div class="mx-lg-5 mb-3">
                                    <label for="password" class="small">Digita la password</label>
                                    <input type="password" class="form-control" id="name" placeholder="">
                                </div>
                                <div class="form-check mx-lg-5">
                                    <input class="form-check-input" type="checkbox" value="" id="remember">
                                    <label class="small" for="remember">
                                        Ricordami
                                    </label>
                                </div>
                                <div class="text-center">
                                    <a href="#" class="btn btn-primary mt-3" data-bs-toggle="modal" data-bs-target="#staticBackdrop">Entra</a>
                                </div>
                                <div class="mx-lg-5 my-4 text-center small">
                                    Hai dimenticato la password? <a href=""><u>Clicca qui</u></a>
                                </div>
                            </div>

                            <div class="card border-0 p-0 bg-light">
                                <div class="card-body p-0">
                                    <div class="p-3 px-5 text-center">
                                        <div class="h2">Non sei ancora registrato?</div>
                                        <a href="#" class="btn btn-primary mt-3" data-bs-toggle="modal" data-bs-target="#staticBackdrop">Registrati adesso</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-0"></div>
            </div>
        </div>
    </section>

    @component('components.rules')
	@endcomponent

@endsection
