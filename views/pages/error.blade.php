@extends('layouts.main')

@section('content')

    <section class="position-relative">
        <div class="container">
            <div class="row mb-5 py-5">

                <div class="col-12 text-center py-5">
                    <img src="{!! Boot::acf()->options->error->image->url ?? ifb() !!}" class="image404" alt="{!! Boot::acf()->options->error->image->alt ?? tfb(1) !!}">

                    <p class="h1 my-3 text-center">
                        {!! Boot::acf()->options->error->title ?? tfb(3) !!}
                    </p>

                    <p class="text-center">
                        {!! Boot::acf()->options->error->text ?? tfb(24) !!}
                    </p>
                </div>
                
            </div>
        </div>
    </section>

@endsection
