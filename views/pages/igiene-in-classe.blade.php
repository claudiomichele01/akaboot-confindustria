@extends('layouts.main')

@section('content')

	@component('components.ending-banner')
	@endcomponent

	@component('components.upperContent', [ "args" => [
		"title" => Boot::acf()->acf->band_title ?? tfb(4),
	]])
	@endcomponent

	<section class="py-5">
        <div class="container">
			<div class="row mx-lg-5 mx-2">

				<div class="col-12 col-lg-6 mb-lg-0 mb-4">
					<div class="position-relative ratio ratio-1x1">
						<img src="{!! Boot::acf()->acf->first_section->image->url ?? ifb() !!}" style="object-fit: contain;" class="images" alt="{!! Boot::acf()->acf->first_section->image->title ?? tfb(1) !!}">
					</div>
				</div>

				<div class="col-12 col-lg-6">
					<div class="h1">{!! Boot::acf()->acf->first_section->title ?? tfb(4) !!}</div>
					<div class="h5 mt-3">
						{!! Boot::acf()->acf->first_section->subtitle ?? tfb(25) !!}
					</div>
					<div class="mt-3">
						{!! Boot::acf()->acf->first_section->text ?? tfb(100) !!}
					</div>
                    <a href="{{route("reserved_area.login")}}" class="btn btn-primary hover mt-5">{!! Boot::acf()->acf->first_section->button->title ?? tfb(2) !!}</a>
				</div>

			</div>
		</div>
    </section>

    <section class="py-5">
        <div class="container">
            <div class="row mx-lg-5 mx-2 text-center">
                <div class="col-12 mb-3">
                    <div class="h2">{!! Boot::acf()->acf->rules->title ?? tfb(6) !!}</div>
                </div>
				@foreach( Boot::acf()->acf->rules->rules as $rul )
					<div class="col-lg-3 col-6 mb-4 mb-lg-0">
						<div class="position-relative ratio ratio-1x1">
							<img src="{!! $rul->image->url ?? ifb() !!}" class="images p-lg-4 pb-0" style="object-fit: contain" alt="{!! $rul->image->title ?? tfb(1) !!}">
						</div>
						<div>
							<div class="h3 mt-lg-n5">{!! $rul->title ?? tfb(3) !!}</div>
							<div class="small px-lg-4">{!! $rul->text ?? tfb(9) !!}</div>
						</div>
					</div>
				@endforeach
            </div>
        </div>
    </section>

	<section class="text-center py-5">
		<img src="{!! Boot::acf()->acf->rules->arrow->url ?? ifb() !!}" style="object-fit: none;" alt="{!! Boot::acf()->acf->rules->arrow->title ?? tfb(1) !!}">
	</section>

    <section class="py-5">
        <div class="container">
			<div class="row mx-lg-5 mx-2 py-3">
                <div class="col-12 text-center">
                    <div class="h2">{!! Boot::acf()->acf->cards_section->title ?? tfb(6) !!}</div>
                </div>
			</div>
			<div class="row py-3 mx-lg-5 mx-2">
				<div class="card border-0 cardShadow">
					<div class="card-body m-lg-3">
						<div class="row">
							<div class="col-12 col-lg-6 pe-lg-5 my-auto">
								<div class="h1">{!! Boot::acf()->acf->cards_section->first_card->title ?? tfb(5) !!}</div>
								<div class="mt-3">
									{!! Boot::acf()->acf->cards_section->first_card->text ?? tfb(61) !!}
								</div>
							</div>
							<div class="col-12 col-lg-6 mt-lg-0 mt-4">
								<div class="position-relative ratio ratio-4x3">
									<img src="{!! Boot::acf()->acf->cards_section->first_card->image ?? ifb() !!}" class="images" style="border-radius: 1.5rem" alt="{!! Boot::acf()->acf->cards_section->first_card->image->title ?? tfb(1) !!}">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row py-5 mx-lg-5 mx-2">

				<div class="card border-0 cardShadow">
					<div class="card-body m-lg-3">
						<div class="row">
							<div class="col-12 col-lg-6 mt-lg-0 my-4 order-lg-1 order-2">
								<div class="position-relative ratio ratio-4x3">
									<img src="{!! Boot::acf()->acf->cards_section->second_card->image->url ?? ifb() !!}" class="images" style="border-radius: 1.5rem" alt="{!! Boot::acf()->acf->cards_section->second_card->image->title ?? tfb(1) !!}">
								</div>
							</div>
							<div class="col-12 col-lg-6 ps-lg-5 my-auto order-lg-2 order-1">
								<div class="h1">{!! Boot::acf()->acf->cards_section->second_card->title ?? tfb(5) !!}</div>
								<div class="mt-3">
									{!! Boot::acf()->acf->cards_section->second_card->text ?? tfb(61) !!}
								</div>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
    </section>

    <section class="py-5">
        <div class="container-fluid px-0">
			<hr class="py-1 gradientBg" style="opacity: 1;">
			<div class="row mx-lg-5 mx-2 py-5">
                <div class="col-lg-3 d-none d-lg-block">
					<div class="position-relative ratio ratio-1x1">
						<img src="{!! Boot::acf()->acf->cta->first_image->url ?? ifb() !!}" class="images" alt="">
					</div>
				</div>
				<div class="col-12 col-lg-6 text-center my-auto">
					<div class="h1">{!! Boot::acf()->acf->cta->title ?? tfb(6) !!}</div>
					<div class="h5 mt-4">
						{!! Boot::acf()->acf->cta->first_text ?? tfb(16) !!}
					</div>
					<div class="h5 my-4">
						{!! Boot::acf()->acf->cta->second_text ?? tfb(34) !!}
					</div>
					<a href="{{route("registration")}}" class="btn btn-primary">{!! Boot::acf()->acf->cta->button->title ?? tfb(2) !!}</a>
				</div>
				<div class="col-12 col-lg-3 mt-4 mt-lg-0">
					<div class="position-relative ratio ratio-1x1">
						<img src="{!! Boot::acf()->acf->cta->second_image->url ?? ifb() !!}" class="images" alt="">
					</div>
				</div>

			</div>
		</div>
    </section>

	@component('components.rules')
	@endcomponent

@endsection
