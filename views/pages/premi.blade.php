@extends('layouts.main')

@section('content')

	@component('components.ending-banner')
	@endcomponent

	@component('components.upperContent', [ "args" => [
		"title" => Boot::acf()->acf->band_title ?? tfb(5),
	]])
	@endcomponent

    <section class="py-5">
        <div class="container px-lg-5">
            <div class="row mx-lg-5 mx-2 text-center">
                <div class="h2">{!! Boot::acf()->acf->first_section->section_title ?? tfb(13) !!}</div>
            </div>
        </div>
    </section>

	<section class="text-center">
		<img src="{!! Boot::acf()->acf->first_section->arrow->url ?? ifb() !!}" style="object-fit: none;" alt="{!! Boot::acf()->acf->first_section->arrow->title ?? tfb(1) !!}">
	</section>

	<section class="py-5">
        <div class="container">
			<div class="row mx-lg-5 mx-2">

				<div class="col-12 col-lg-6 pe-lg-5 my-auto">
					<div class="h1">{!! Boot::acf()->acf->first_section->title ?? tfb(4) !!}</div>
                    <div class="my-3 h5" style="font-weight: 400">
						{!! Boot::acf()->acf->first_section->subtitle ?? tfb(15) !!}
					</div>
					<div class="">
						{!! Boot::acf()->acf->first_section->text ?? tfb(47) !!}
					</div>
				</div>

				<div class="col-12 col-lg-6">
					<div class="position-relative ratio ratio-16x9 mt-lg-0 mt-4">
						<img src="{!! Boot::acf()->acf->first_section->image->url ?? ifb() !!}" class="images" alt="{!! Boot::acf()->acf->first_section->image->title ?? tfb(1) !!}">
					</div>
				</div>

			</div>
		</div>
    </section>

    <section class="py-5 bg-gray">
        <div class="container">
			<div class="row mx-lg-5 mx-2">

				<div class="col-12 col-lg-6 order-lg-1 order-2">
					<div class="position-relative ratio ratio-16x9 mt-lg-0 mt-4">
						<img src="{!! Boot::acf()->acf->second_section->image->url ?? ifb() !!}" class="images" alt="{!! Boot::acf()->acf->second_section->image->title ?? tfb(1) !!}">
					</div>
				</div>

                <div class="col-12 col-lg-6 order-lg-2 order-1 pe-lg-5 my-auto">
					<div class="h1">{!! Boot::acf()->acf->second_section->title ?? tfb(4) !!}</div>
					<div class="my-3">
						{!! Boot::acf()->acf->second_section->subtext ?? tfb(27) !!}
					</div>
					<div class="h5" style="font-weight: 400">
						{!! Boot::acf()->acf->second_section->text ?? tfb(23) !!}
					</div>
				</div>

			</div>
		</div>
    </section>

	<section class="text-center py-5">
		<img src="{!! Boot::acf()->acf->first_section->arrow->url ?? ifb() !!}" style="object-fit: none;" alt="{!! Boot::acf()->acf->first_section->arrow->title ?? tfb(1) !!}">
	</section>

    <section class="pb-5">
        <div class="container">
            <div class="row mx-lg-5 mx-2">
				
				<div class="card border-0 gradientBg">
					<div class="card-body m-lg-3">
						<div class="row">
							<div class="col-12 text-center">
								<div class="h1 text-light">{!! Boot::acf()->acf->third_section->card->title ?? tfb(4) !!}</div>
								<div class="h5 mx-lg-5 py-2 text-light">{!! Boot::acf()->acf->third_section->card->text ?? tfb(19) !!}</div>
								<a href="{{route("reserved_area.login")}}" class="btn btn-light text-primary mt-3">{!! Boot::acf()->acf->third_section->card->button->title ?? tfb(2) !!}</a>
							</div>
						</div>
					</div>
				</div>

            </div>
        </div>
    </section>

	<section class="py-5">
        <div class="container">
			<div class="row mx-lg-5 mx-2">
				<div class="col-12">
					<div class="position-relative ratio ratio-21x9 mt-lg-0 mt-4">
						<img src="{!! Boot::acf()->acf->third_section->image->url ?? ifb() !!}" style="object-fit: contain;" class="images" alt="{!! Boot::acf()->acf->third_section->image->title ?? tfb(1) !!}">
					</div>
				</div>
			</div>
		</div>
    </section>

	@component('components.rules')
	@endcomponent

@endsection
