<section>
    <h1>Ciao {{ $name }},</h1>
    <div>
        <p>per confermare la registrazione come docente al progetto DivertiDenti, clicca sul link di seguito:</p>
        <a href="{{$url}}">Crea la tua password</a>
    </div>
    <div>
        <p>I materiali educativi gratuiti ti aspettano nell’Area docente, a cui potrai accedere dalla sezione “Progetto scuola”.</p>
    </div>
    <br>
    <i style="font-size: 10px">[Se pensi di aver ricevuto questo messaggio per errore, ignoralo e non accadrà nulla.]</i>
    <br>
    <p>
        Per informazioni sul progetto educativo e la piattaforma DivertiDenti
        Numero verde 800.17.25.34 attivo dal lunedì al venerdì dalle 9 alle 13
        Email progettoscuola@divertidenti.it
    </p>
</section>