<section>
    <h1>Non è stato trovato un istituto</h1>
    <p>
        L'utente {{ $name }},<br>
        non è riuscito a trovare la scuola : {{ $school_name }} di {{$school_city}}.
        @if(!empty($school_code))
            Il codice meccanografico della scuola è : {{$school_code}}
        @endif
    </p>
    <div>
        <p>Di seguito i contatti del docente:</p>
        Email : <a href="mailto:{{$email}}">{{$email}}</a>
    </div>
</section>