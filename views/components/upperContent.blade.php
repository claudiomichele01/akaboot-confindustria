@php
    $args = (object)$args;
@endphp

<section class="bg-gray py-4 gradientBg">
    <div class="container">
        <div class="row mx-lg-5 mx-2">
            <div class="col-12 text-center">
                <div class="h2 text-white mb-0">{!! $args->title !!}</div>
            </div>
        </div>
    </div>
</section>