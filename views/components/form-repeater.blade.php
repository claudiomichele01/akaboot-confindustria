@php
    $args = (object)$args;
@endphp

<div id="{!! $args->id !!}" class="aka-repeatable" style="display: none">
    {!! $repeater_content !!}
</div>
<div class="aka-repeater-footer text-center">
    {!! $repeater_footer !!}
</div>

@push('footer')
    <script>
        jQuery( function($) {
            var $repeater = $('#{!! $args->id !!}.aka-repeatable');

            var cloneCount = 1;
            var getClone = function() {
                $cloned = $repeater.clone();

                var $inputs = $cloned.find(':input');

                //Makes repeater defaults
                $inputs.each( function( index, elem ) {
                    var $elem = $(elem);
                    $elem.attr('name', '{!! $args->id !!}[' + cloneCount + '][' + $elem.attr('name') + ']' );
                });

                $cloned.find('.repeater-remove-item').on('click', function() {
                    $this = $(this).closest('.aka-repeatable');
                    $this.fadeOut( function() {
                        $this.detach();
                    });
                });

                cloneCount++;
                return $cloned;
            };

            var $addNewButton = $repeater.next('.aka-repeater-footer');
            $addNewButton.on('click', function() {
                var $addButton = $(this);
                $addButton.before( getClone().fadeIn() );
            });

            $cloned = getClone();
            $cloned.insertBefore( $addNewButton ).show();
        });
    </script>
@endpush
