@switch($args['mode'])
    @case('double+single')
        <div class="form-row row ">
            <div class="col-lg-6 col-12">
                {!! $c1 !!}
            </div>
            <div class="col-lg-6 col-12">
                {!!$c2 !!}
            </div>
        </div>
        <div class="form-row row mb-4">
            <div class="col-12">
                {!! $c3 !!}
            </div>
        </div>
    @break
@endswitch
