<section style="background-color: #EAEBF2">
    <div class="container">
        <div class="row text-center pt-4">
            <div class="small"><strong>{!! Boot::acf()->options->rules->text ?? tfb(3) !!}</strong></div>
        </div>
        <div class="row py-2">
            <div class="col-0 col-lg-3"></div>
            <div class="col-12 col-lg-3 my-lg-auto">
                <div class="h2 text-lg-end text-center mb-0">
                    <i class="bi bi-telephone-fill h4 text-primary"></i>
                    <a href="tel:{!! Boot::acf()->options->rules->number ?? tfb(1) !!}" style="text-decoration:none" class="text-primary">{!! Boot::acf()->options->rules->number ?? tfb(1) !!}</a>
                </div>
            </div>
            <div class="col-12 col-lg-3 text-lg-start text-center my-auto">
                <div class="small pe-lg-5"><i>{!! Boot::acf()->options->rules->next_to_number_text ?? tfb(11) !!}</i></div>
            </div>
            <div class="col-0 col-lg-3"></div>
        </div>
        <div class="row text-center pb-4">
            <div class="h6 mt-3 mt-lg-0">{!! Boot::acf()->options->rules->up_to_email ?? tfb(3) !!}</div>
            <a href="mailto:{!! Boot::acf()->options->rules->email ?? tfb(1) !!}" class="text-primary">{!! Boot::acf()->options->rules->email ?? tfb(1) !!}</a>
            <div class="py-3 small">
                <i>
                    {!! Boot::acf()->options->rules->disclaimer ?? tfb(48) !!}
                </i>
            </div>
            <a href="{!! Boot::childUrl("downloads") !!}/Regolamento_DIVERTIDENTI_22_23.pdf">{!! Boot::acf()->options->rules->rules->title ?? tfb(1) !!}</a>
        </div>
    </div>
</section>