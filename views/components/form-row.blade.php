@php
    $args = (object)$args;
@endphp

<div class="{!! $args->name !!}">

    @component('components.form-cols', [ "args" => [
        "mode" => "double+single"
      ]])
        @slot('c1')
            <div class="class_repeater">
                <label for="class" class="small">Classe</label>
                <select id="class" name="class" class="form-select mb-3" aria-label="form-select-lg" >
                    <option disabled selected value="">---</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
            </div>
        @endslot

        @slot('c2')
            <div class="section_repeater">
                <label for="section" class="small">Sezione</label>
                <select class="form-select mb-3" aria-label="form-select-lg"  name="section">
                    <option disabled selected value="">---</option>
                    <option value="A">A</option>
                    <option value="B">B</option>
                    <option value="C">C</option>
                    <option value="D">D</option>
                    <option value="E">E</option>
                    <option value="F">F</option>
                    <option value="G">G</option>
                    <option value="H">H</option>
                    <option value="I">I</option>
                    <option value="L">L</option>
                    <option value="M">M</option>
                    <option value="N">N</option>
                    <option value="O">O</option>
                    <option value="P">P</option>
                    <option value="Q">Q</option>
                    <option value="R">R</option>
                    <option value="S">S</option>
                    <option value="T">T</option>
                    <option value="U">U</option>
                    <option value="V">V</option>
                    <option value="Z">Z</option>
                </select>
            </div>
        @endslot

        @slot('c3')
            <div class="title_repeater">
                <label for="title" class="small">Scrivi il nome della classe*</label>
                <input id="title" name="title" type="text" class="form-control mb-3">
            </div>
        @endslot
    @endcomponent

</div>
