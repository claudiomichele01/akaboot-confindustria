@php
    $args = (object)$args;
    $games = $args->games ?? false;
    $button_text = $args->button_text ?? false;
    $uppertitle = $args->uppertitle ?? false;
@endphp

<div class="{!! $args->layout !!}">
    <div class={{$games ? "gamesCard" : ""}}>
        <div class="card contentCard border-0 cardShadow">
            <div class="card-body text-center p-0 d-flex flex-column">
                <div class="position-relative ratio ratio-16x9">
                    <img src="{!! $args->image !!}" class="singleCardImage" alt="">
                </div>
                <div class="p-3">
                    @if($uppertitle)
                        <div class="mb-2 cardTextFont">{!! $uppertitle !!}</div>
                    @endif
                    <div class="h5 card-title">{!! $args->title !!}</div>
                    <hr class="hr">
                    <div class="card-text cardTextFont mx-2">{!! $args->text !!}</div>
                </div>
                @if($button_text)
                    <a href="{!! $args->link !!}" class="btn btn-primary mt-auto mb-3 mx-5">{!! $button_text !!}</a>
                @endif
            </div>
        </div>
    </div>
</div>