<div class="side-buttons-container position-fixed">
    <div class="side-buttons d-flex flex-column align-items-end">
        {{-- questi bottoni devono andare alle altre città, quindi devono essere variabli i link --}}
        <div>
            <a href="#" class="side-btn">
            <span class="side-button-text">Città #1</span>
            <span class="side-button-icon">
                <i class="icon-star"></i>
            </span>
            </a>
        </div>
        <div>
            <a href="#" class="side-btn">
            <span class="side-button-text">Città #2</span>
            <span class="side-button-icon">
                <i class="icon-star"></i>
            </span>
            </a>
        </div>
        <div>
            <a href="#" class="side-btn">
            <span class="side-button-text">Città #1</span>
            <span class="side-button-icon">
                <i class="icon-star"></i>
            </span>
            </a>
        </div>
        <div>
            <a href="#" class="side-btn">
            <span class="side-button-text">Città #2</span>
            <span class="side-button-icon">
                <i class="icon-star"></i>
            </span>
            </a>
        </div>
    </div>
</div>
  
<div class="btn-add-container position-fixed">
    <a href="#" class="side-btn">
        <span class="side-button-text">Carica file</span>
        <span class="side-button-icon">
            <i class="icon-plus"></i>
        </span>
    </a>
</div>