@extends('layouts.fullwidth')

@section('fullwidthContent')

	<main class="position-relative">

		@include('layouts.components.header')

		@yield('content')

		@include('layouts.components.footer')

	</main>

@endsection
