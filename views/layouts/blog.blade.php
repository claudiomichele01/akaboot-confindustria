@extends('layouts.main')

@section('content')

<section class="container py-4">

    @yield('blog_hero')

    <div class="row">
        <div class="col-lg-8 mb-4 mb-lg-0">

            @if( !($hideTitle ?? false) )
                <div class="section-header mb-4">
                    <h1 class="section-title h4 text-uppercase pb-2 mb-0"><span>{!! Boot::acf()->wp->term->name ?? "News" !!}</span></h1>
                </div>
            @endif

            <div class="row">

                @yield('author_hero')
                
                @yield('blog_content')

            </div>
        </div>

        <div class="col-lg-4">

            @include('layouts.components.sidebar')

        </div>
    </div>
</section>

@endsection
