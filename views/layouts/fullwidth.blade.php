<!doctype html>

<html {!! Boot::htmlAttrs() !!}>

	<head>

			{{-- Codice in Head --}}
			{!! Boot::acf()->options->monitoraggio->codice_in_head ?? "" !!}
			<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
			<script id="Cookiebot" src="https://consent.cookiebot.com/uc.js" data-cbid="78972da1-4dd8-434a-8f3e-697681b32686" data-blockingmode="auto" type="text/javascript"></script>

			@if(Boot::isProduction())
				<!-- Meta Pixel Code -->
				<script>
					!function(f,b,e,v,n,t,s)
					{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
							n.callMethod.apply(n,arguments):n.queue.push(arguments)};
						if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
						n.queue=[];t=b.createElement(e);t.async=!0;
						t.src=v;s=b.getElementsByTagName(e)[0];
						s.parentNode.insertBefore(t,s)}(window, document,'script',
							'https://connect.facebook.net/en_US/fbevents.js');
					fbq('init', '515220333317483');
					fbq('track', 'PageView');
				</script>
				<noscript><img height="1" width="1" style="display:none"
						   src="https://www.facebook.com/tr?id=515220333317483&ev=PageView&noscript=1"
				/></noscript>
				<!-- End Meta Pixel Code -->

				<!-- Matomo -->
				<script>
					var _paq = window._paq = window._paq || [];
					/* tracker methods like "setCustomDimension" should be called before "trackPageView" */
					_paq.push(['trackPageView']);
					_paq.push(['enableLinkTracking']);
					(function() {
						var u="https://def.matomo.cloud/";
						_paq.push(['setTrackerUrl', u+'matomo.php']);
						_paq.push(['setSiteId', '2']);
						var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
						g.async=true; g.src='//cdn.matomo.cloud/def.matomo.cloud/matomo.js'; s.parentNode.insertBefore(g,s);
					})();
				</script>
				<!-- End Matomo Code -->
			@endif

			@stack('head')
		{{-- End Codice in Head --}}

        {{-- Required meta tags --}}
        	{!! Boot::headMetas() !!}

			<meta property="og:locale" content="it_IT" /> <meta property="og:type" content="website" />
			<meta property="og:title" content="Divertidenti" />
			<meta property="og:description" content="Divertidenti" /> 
			<meta property="og:url" content="{!! home_url() !!}" />
			<meta property="og:site_name" content="Divertidenti" /> 
			<meta property="article:publisher" content="{!! home_url() !!}" /> <meta property="og:image:secure" content="{!! home_url() !!}/wp-content/themes/divertidenti/images/Img_condivisione_1920x1080.jpg" />
			<meta property="og:image" content="{!! home_url() !!}/wp-content/themes/divertidenti/images/Img_condivisione_1920x1080.jpg" />
			<meta property="og:image:width" content="1080" />
			<meta property="og:image:height" content="1080" />
		{{-- End Required meta tags --}}

        {{-- Favicon --}}
			<link rel="apple-touch-icon" sizes="180x180" href="{!! Boot::childUrl('images/favicon/apple-touch-icon.png') !!}">
			<link rel="icon" type="image/png" sizes="32x32" href="{!! Boot::childUrl('images/favicon/favicon-32x32.png') !!}">
			<link rel="icon" type="image/png" sizes="16x16" href="{!! Boot::childUrl('images/favicon/favicon-16x16.png') !!}">
			<link rel="manifest" href="{!! Boot::childUrl('images/favicon/site.webmanifest') !!}">
			<link rel="mask-icon" href="{!! Boot::childUrl('images/favicon/safari-pinned-tab.svg') !!}" color="#2566AF">
			<link rel="shortcut icon" href="{!! Boot::childUrl('images/favicon/favicon.ico') !!}">
			<meta name="apple-mobile-web-app-title" content="DivertiDenti">
			<meta name="application-name" content="DivertiDenti">
			<meta name="msapplication-TileColor" content="#ffffff">
			<meta name="msapplication-config" content="{!! Boot::childUrl('images/favicon/browserconfig.xml') !!}">
			<meta name="theme-color" content="#ffffff">

		{{-- End Favicon --}}
			<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />


			{!! Boot::headAssets() !!}
			{!! Boot::headTitle() !!}
		@php 
			$data_incorporated = ['reactConst' => REACT_CONST];
   			 $react_data = json_encode( $data_incorporated );
    
    		echo "<script> sessionStorage.setItem('childThemeData', '$react_data') </script>";
		@endphp
		
		@include('layouts.components.critical-css-path')
    </head>

    <body {!! Boot::bodyAttrs() !!}>
		<div id ="modalHook"></div>

		{{-- Codice nel Body --}}
		{!! Boot::acf()->options->monitoraggio->codice_nel_body ?? "" !!}
		{{-- End Codice nel Body --}}

		{{-- Content --}}

			@yield('fullwidthContent')

		{{-- End Content --}}
		{!! Boot::footerAssets() !!}

		<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
		@stack('footer')
	</body>

</html>
