@php
    use AkaSchool\Models\User;

    $user = Boot::getloggedUser();
    $menu = Boot::getMenu('menu-principale');

    if ( $user ) {
        $user = User::find( $user );
        $user_meta = get_userdata($user->ID);
        $user_roles = $user_meta->roles;
        $user_is_teacher = count($user_roles) > 0 && $user_roles[0] == 'teacher';
        array_pop($menu[0]->children);
    } else{
        $user_is_teacher = false;
    }

@endphp

<header class="sticky-top bg-white">

    @component('backoffice.components.teacher.topBar', [ "args" => [
        "user" => $user,
        "isTeacher" => $user_is_teacher,
        "background" => "",
        "TextColor" => "",
    ]])
    @endcomponent

    <nav class="navbar nav navbar-expand-lg navbar-light bg-white">
        <div class="container-fluid px-lg-5">
            <a class="navbar-brand me-0" href="{!! home_url() !!}" style="width: 50%">
                <img src="{!! Boot::acf()->options->akaboot_main_settings->logo_light->url ?? ifb() !!}" class="logo">
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ms-lg-auto text-center text-lg-start py-4 py-lg-2">
                    @foreach ( $menu as $entry )

                        @if( !$entry->hasChildren )
                            <li class="nav-item{{ $entry->isActive ? " active" : "" }}">
                                <a href="{{ $entry->url }}" class="nav-link font-weight-bold">{{ $entry->title }}</a>
                            </li>
                        @else
                            <li class="nav-item dropdown px-2 {{ $entry->isActive ? " active" : "" }}">
                                <a class="nav-link dropdown-toggle pb-0 text-secondary" href="#" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    {{ $entry->title }}
                                </a>
                                <ul class="dropdown-menu dropBorder border py-0 mt-3" aria-labelledby="navbarDarkDropdownMenuLink">
                                    @foreach ( $entry->children as $child )

                                        <li><a class="dropdown-item py-2 mb-0 text-center text-secondary small" {{ $child->isActive ? " active" : "" }} href="{{ $child->url }}">{{ $child->title }}</a></li>
                                    @endforeach
                                </ul>
                            </li>
                            <hr style="color: #D5D5D5; margin-right: 6rem; margin-left: 6rem" class="d-block d-md-none">
                        @endif

                    @endforeach
                </ul>
            </div>
        </div>
    </nav>
  </header>