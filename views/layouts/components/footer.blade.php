<footer class="py-4" id="footer">
    <div class="container">
      <div class="row">
        <div class="col-12 col-lg-6">
          <img src="{!! Boot::acf()->options->footer->loghi->url ?? ifb() !!}" class="w-75 my-auto" alt="loghi promotori">
        </div>
        <div class="col-12 col-lg-6 text-lg-end mt-3 mt-lg-0 my-auto">
          <a href="/cookie-policy" class="small">Cookie policy</a>          
          <a href="{!! Boot::childUrl("downloads") !!}/DivertiDenti_Privacy Policy.pdf" class="ms-4 small">Privacy policy</a>
        </div>
      </div>
    </div>
  </footer>