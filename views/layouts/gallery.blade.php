@extends('pages.gallery')
@section('gallery-content')

    @component('components.ending-banner')
	@endcomponent

    @component('components.upperContent', [ "args" => [
        "title" => Boot::acf()->options->gallery->topbar ?? tfb(5),
    ]])
    @endcomponent

    <section class="py-5">
        <div class="container">
            <div class="row mx-lg-5 mx-2">
                <div class="col-12 text-center">
                    <div class="h2">{!! Boot::acf()->options->gallery->title ?? tfb(8) !!}</div>
                    <div>
                        {!! Boot::acf()->options->gallery->text ?? tfb(62) !!}
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="" id="galleryReactHook" ></section>

    <section class="text-center">
        <img src="{!! Boot::acf()->options->gallery->arrow->url ?? ifb() !!}" alt="arrow">
    </section>

    <section class="py-5">
		<div class="container">
			<div class="row mx-lg-5 mx-2">

				<div class="card border-0 gradientBg">
                    <div class="card-body m-lg-3 m-1">
                        <div class="row text-center">
                            <div class="col-12">
                                <div class="h1 text-white">{!! Boot::acf()->options->gallery->card->title ?? tfb(9) !!}</div>
                                <div class="h5 py-2 text-white mx-lg-5">
                                    {!! Boot::acf()->options->gallery->card->text ?? tfb(26) !!}
                                </div>
                                <a href="{{route("registration")}}" class="btn btn-light text-primary">{!! Boot::acf()->options->gallery->card->button->title ?? tfb(2) !!}</a>
                                {{-- <a href="{!! Boot::acf()->acf->card->second_card->button->link->url ?? tfb(1) !!}" type="button" class="btn btn-light text-primary">Registrati adesso</a> --}}
                            </div>
                        </div>
                    </div>
                </div>

			</div>
		</div>
	</section>

    <section class="pt-5 pb-3">
		<div class="container">
			<div class="row mx-lg-5 mx-2">
				<div class="ratio ratio-16x9">
                    <img src="{!! Boot::acf()->options->gallery->image->url ?? ifb() !!}" class="images" style="object-fit: contain" alt="{!! Boot::acf()->options->gallery->image->alt ?? tfb(1) !!}">
                </div>
			</div>
		</div>
	</section>

    @component('components.rules')
	@endcomponent

@endsection
