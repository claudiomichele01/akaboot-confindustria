@extends('layouts.main')

@section('content')

    <section class="position-relative vh-100">
        <div class="container">
            <div class="row mb-5 pb-5">

                <div class="col-12 text-center py-5">
                    <img src="{!! Boot::acf()->options->confirm_np_email->image->url ?? ifb() !!}" class="image404" alt="{!! Boot::acf()->options->confirm_np_email->image->url ?? ifb() !!}">
                    <p class="h1 my-3 text-center">
                        {!! Boot::acf()->options->confirm_np_email->title ?? tfb(2) !!}
                    </p>

                    <p class="text-center mx-5">
                        {!! Boot::acf()->options->confirm_np_email->text ?? tfb(11) !!}
                    </p>
                </div>
                
            </div>
        </div>
    </section>

@endsection
