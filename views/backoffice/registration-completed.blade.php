@extends('layouts.main')

@section('content')

    <section class="position-relative vh-100">
        <div class="container">
            <div class="row mb-5 pb-5">

                <div class="col-12 text-center py-5">
                    <img src="{!! Boot::acf()->options->reg_completata->image->url ?? ifb() !!}" class="image404" alt="{!! Boot::acf()->options->reg_completata->image->url ?? ifb() !!}">
                    <p class="h1 my-3 text-center">
                        {!! Boot::acf()->options->reg_completata->title ?? tfb(4) !!}
                    </p>

                    <p class="text-center mx-5">
                        {!! Boot::acf()->options->reg_completata->text ?? tfb(11) !!}
                    </p>
                    <a href="{{route(AKA_TEACHER_PAGE)}}" class="btn btn-primary">{!! Boot::acf()->options->reg_completata->button_text ?? tfb(3) !!}</a>
                </div>
                
            </div>
        </div>
    </section>

@endsection
