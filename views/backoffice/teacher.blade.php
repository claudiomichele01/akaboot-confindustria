@extends('layouts.main')

@section('content')

    @if(!empty($error))
        <div class="row">
            <div class="alert alert-danger mb-0" role="alert">
                Errore: {{$error}}
            </div>
        </div>
    @endif

    @component('components.upperContent', [ "args" => [
		"title" => Boot::acf()->options->teacher->bandtitle ?? tfb(3),
	]])
	@endcomponent

	<section class="py-5">
		<div class="container container-padding">
			<div class="row mx-lg-5 mx-2">
				<div class="col-12 text-center">
					<div class="h2">{!! Boot::acf()->options->teacher->teacher_infos->greetings ?? tfb(1) !!} <span class="text-uppercase">{{$user->display_name}}</span>!</div>
					<div class="h5 py-2">{!! Boot::acf()->options->teacher->teacher_infos->school ?? tfb(7) !!} <span class="text-uppercase">{{$school->institute->name}}</span></div>
				</div>
			</div>
		</div>
	</section>

    <section class="text-center">
        <img src="{!! Boot::acf()->options->teacher->arrow->url ?? ifb() !!}" alt="arrow">
    </section>
    
    <section class="py-5">
        <div class="container container-padding">
            <div class="row mx-lg-5 mx-2">
                <div class="col-12 text-center">
                    <div class="h1">{!! Boot::acf()->options->teacher->first_section->title ?? tfb(6) !!}</div>
                    <div class="p-2">
                        {!! Boot::acf()->options->teacher->first_section->text ?? tfb(32) !!}
                    </div>
                    <a  href="{{route(AKA_TEACHER_PAGE . ".download_starter_kit")}}" class="btn btn-primary mt-3">{!! Boot::acf()->options->teacher->first_section->button->title ?? tfb(5) !!}</a>
                </div>
            </div>
        </div>
    </section>

    <section class="py-5">
		<div class="container container-padding">
			<div class="row mx-lg-5 mx-2">
				<div class="col-12 text-center">
					<div class="h1">{!! Boot::acf()->options->teacher->third_section->title ?? tfb(11) !!}</div>
					<div class="py-3">
                        {!! Boot::acf()->options->teacher->third_section->text ?? tfb(45) !!}
                    </div>
                    <div class="h5">
                        {!! Boot::acf()->options->teacher->third_section->second_text ?? tfb(27) !!}
                    </div>
                    <a href="{{ route(AKA_TEACHER_PAGE . ".download_optional_kit") }}"  class="btn btn-primary mt-5">{!! Boot::acf()->options->teacher->third_section->button->title ?? tfb(6) !!}</a>
                </div>
			</div>
            <div class="row mx-lg-5 mx-2 mt-3">
                <div class="col-lg-2 col-0"></div>
                <div class="col-lg-4 col-6 px-5">
                    <div class="position-relative ratio ratio-1x1">
                        <img src="{!! Boot::acf()->options->teacher->third_section->image->url ?? ifb() !!}" class="images p-lg-4" style="object-fit: contain" alt="">
                    </div>
                </div>
                <div class="col-lg-4 col-6 my-auto">
                    <div>
                        {!! Boot::acf()->options->teacher->third_section->image_description ?? tfb(28) !!}
                    </div>
                </div>
                <div class="col-lg-2 col-0"></div>
            </div>
		</div>
	</section>

    <section class="py-5">
		<div class="container">
			<div class="row mx-lg-5 mx-2">

                {{-- disattivazione / chiusura concorso --}}
                {{-- TODOCLAUDIO: questo componente va adeguato per essere riciclabile di progetto in progetto --}}
                @include('backoffice.components.teacher.classes-card', [ "args" => [
                    "title" => Boot::acf()->options->teacher->fourth_section->title ?? tfb(9),
                    "titleClasses" => "text-white",
                    
                    "text" => Boot::acf()->options->teacher->fourth_section->text ?? tfb(38),
                    "textClasses" => "text-white",

                    "btnText" => 'CARICA CONTRIBUTI',
                    "btnClasses" => "btn-primary border-0 gradientBg",
                    "bntCss" => "",

                    "parentCardClasses" => "cardShadow border-0",
                    "parentCardCss" => "",

                    "childCardClasses" => "border-0 gradientBg",
                    "childCardCss" => "",

                    "classes" => $classes,
                ]])

			</div>
		</div>
	</section>

    <section class="py-5">
        <div class="container container-padding">
            
            @include('backoffice.components.teacher.add-class' , ["institute" => $school->institute])

        </div>
    </section>
    
    @component('components.rules')
	@endcomponent

@endsection