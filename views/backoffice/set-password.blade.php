@extends('layouts.main')

@section('content')

    <section class="position-relative">
        <div class="container">

            <div class="row pt-5">
                <div class="col-lg-3 col-0"></div>
                <div class="col-lg-6 col-12 pt-5">
                    <p class="h1 my-3 text-center">
                        {!! Boot::acf()->options->set_password->title ?? tfb(4) !!}
                    </p>

                    <p class="text-center h5">
                        {!! Boot::acf()->options->set_password->text ?? tfb(17) !!}
                    </p>
                </div>
                <div class="col-lg-3 col-0"></div>
            </div>

            
            <div class="row my-3">
                <div class="col-lg-3 col-0"></div>
                <div class="col-lg-6 col-12 pt-5">
                    @if(!empty($error))
                        <div class="row my-3">
                            <div class="alert-danger px-2">
                                {{$error}}
                            </div>
                        </div>
                    @endif
                </div>
                <div class="col-lg-3 col-0"></div>
            </div>


            <form method="POST" action="{{route("account.change_user_pass")}}">
                <input type="hidden" name="nonce" value={{ wp_create_nonce('sv_api_validation') }}>
                <input type="hidden" name="user_id" value={{ $user->id }}>
                <div class="row pb-5 mb-5 mt-3">
                    <div class="col-lg-4 col-0"></div>
                    <div class="col-lg-4 col-12 pb-5">
                        <label for="password" class="small text-start">La tua password</label>
                        <div class="input-group mb-3">
                            <input type="password" name="pass" class="form-control" id="password" placeholder="Digita la password" required>
                            <div class="input-group-append">
                                <span class="input-group-text" id="togglePassword">
                                    <i class="bi bi-eye" ></i>
                                </span>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="passwordConfirm" class="small text-start">Conferma</label>
                            <div class="input-group mb-3">
                                <input type="password" name="conf_pass" class="form-control" id="passwordConfirm" placeholder="Per sicurezza, ripeti la password" required>
                                <div class="input-group-append">
                                    <span class="input-group-text" id="toggleConfirmPassword">
                                        <i class="bi bi-eye" ></i>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="text-center">
                            <button class="btn btn-primary mt-3" type="submit">Salva password</button>
                        </div>

                    </div>
                    <div class="col-lg-4 col-0"></div>
                </div>
            </form>

        </div>
    </section>

@endsection

@push("footer")
    <script>
        const togglePassword = document.querySelector('#togglePassword');
        const password = document.querySelector('#password');

        togglePassword.addEventListener('click', function (e) {
            // toggle the type attribute
            const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
            password.setAttribute('type', type);
            // toggle the eye slash icon
            this.classList.toggle('fa-eye-slash');
        });

        const toggleConfirmPassword = document.querySelector('#toggleConfirmPassword');
        const confirmPassword = document.querySelector('#passwordConfirm');

        toggleConfirmPassword.addEventListener('click', function (e) {
            // toggle the type attribute
            const type = confirmPassword.getAttribute('type') === 'password' ? 'text' : 'password';
            confirmPassword.setAttribute('type', type);
            // toggle the eye slash icon
            this.classList.toggle('fa-eye-slash');
        });


        jQuery(function ($) {
            $(document).ready(function () {
                var isLock = false;
                $("form").on('submit', function (e) {
                    if (isLock)
                        return false;

                    isLock = true;

                    $('button[type="submit"]').attr('disabled', true);
                });
            });
        });

    </script>
@endpush