@extends('layouts.main')

@section('content')

    @component('components.ending-banner')
	@endcomponent

    <section class="py-3 gradientBg">
        <div class="container py-3">
            <div class="row mx-lg-5 mx-2">
                <div class="col-lg-3 col-0"></div>
                <div class="col-lg-6 col-12">
                    <div class="card border-0">
                        <div class="card-body p-0">
                            <div class="p-3 pb-1 px-lg-5">
                                <div class="text-center">
                                    <div class="h2 mt-2">Sei un docente?</div>
                                    <div class="h1">ACCEDI</div>
                                </div>

                                <form action="{{route("reserved_area.enter")}}" method="POST">
                                    <div class="mx-lg-5 my-3">
                                        Inserisci email e password per accedere alla tua area riservata per scoprire i contenuti e partecipare all’iniziativa.
                                    </div>

                                    @if(!empty($error))
                                        <div class="alert-danger mx-5 mb-3 px-1">
                                            {{$error}}
                                        </div>
                                    @endif

                                    <div class="mx-lg-5 mb-3">
                                        <label for="email" class="small">Scrivi la tua email</label>
                                        <input type="text" class="form-control" name="log" id="name" placeholder="">
                                    </div>
                                    <div class="mx-lg-5 mb-3">
                                        <label for="password" class="small">Digita la password</label>
                                        <div class="input-group">
                                            <input type="password" name="pwd" class="form-control" id="password" placeholder="">
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="togglePassword">
                                                    <i class="bi bi-eye" ></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-check mx-lg-5">
                                        <input class="form-check-input" name="remember_me" type="checkbox" value="" id="remember">
                                        <label class="small" for="remember">
                                            Ricordami
                                        </label>
                                    </div>
                                    <div class="text-center">
                                        <button class="btn btn-primary mt-3">Entra</button>
                                    </div>
                                </form>

                                <div class="mx-lg-5 my-3 text-center small">
                                    {{-- TODO: Costrutire un sistema di recupero password --}}
                                    Hai dimenticato la password? <a href="{{route("reset_password")}}"><u>Clicca qui</u></a>
                                </div>
                            </div>

                            {{-- disattivazione / chiusura concorso --}}
                            <div class="card border-0 p-0" style="background-color: #EAEBF2">
                                <div class="card-body p-0">
                                    <div class="p-3 px-5 text-center">
                                        <div class="h2">Non sei ancora registrato?</div>
                                        <a href="{{route("registration")}}" class="btn btn-primary mt-3">Registrati adesso</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-0"></div>
            </div>
        </div>
    </section>

    @component('components.rules')
	@endcomponent

@endsection

@push("footer")
    <script>
        const togglePassword = document.querySelector('#togglePassword');
        const password = document.querySelector('#password');

        togglePassword.addEventListener('click', function (e) {
            // toggle the type attribute
            const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
            password.setAttribute('type', type);
            // toggle the eye slash icon
            this.classList.toggle('fa-eye-slash');
        });
    </script>
@endpush
