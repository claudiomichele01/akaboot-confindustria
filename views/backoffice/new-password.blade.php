@extends('layouts.main')

@section('content')

    <section class="position-relative">
        <div class="container">

            <div class="row pt-5">
                <div class="col-lg-3 col-0"></div>
                <div class="col-lg-6 col-12 pt-5">
                    <p class="h1 my-3 text-center">
                        {!! Boot::acf()->options->new_password->title ?? tfb(5) !!}
                    </p>

                    <p class="text-center h5">
                        {!! Boot::acf()->options->new_password->text ?? tfb(17) !!}
                    </p>
                </div>
                <div class="col-lg-3 col-0"></div>
            </div>

            <div class="row pb-5 mb-5 mt-3">
                <div class="col-lg-4 col-0"></div>
                <div class="col-lg-4 col-12 pb-5">

                    <form name="resetpassform" id="resetpassform" action="{{route("store_password")}}" method="post" autocomplete="off">
                        <input type="hidden" id="user_login" name="user_login" value="{{ $user_row_url }}" autocomplete="off">
                        {{-- qui sopra ci va l'email dell'utente nel value --}}
            
                        <div class="user-pass1-wrap">
                            <label for="pass1">Nuova password</label>

                            <div class="input-group mb-3">
                                <input  id="password" type="password" class="form-control" data-reveal="1" data-pw="{{ esc_attr( wp_generate_password( 16 ) ) }}" name="pass1" id="pass1" class="input password-input hide-if-no-js strong" value="" autocomplete="off" aria-describedby="pass-strength-result" placeholder="Digita la password">

                                <div class="input-group-append">
                                    <span class="input-group-text" id="togglePassword">
                                        <i class="bi bi-eye" ></i>
                                    </span>
                                </div>

                            </div>
                        </div>
            
                        <div class="user-pass2-wrap mt-4">

                            <label for="pass2">Conferma</label>

                            <div class="input-group mb-3">
                                <input type="password" class="form-control" id="passwordConfirm"  name="pass2" id="pass2" class="input" value="" autocomplete="off" placeholder="Per sicurezza, ripeti la password">

                                <div class="input-group-append">
                                    <span class="input-group-text" id="toggleConfirmPassword">
                                        <i class="bi bi-eye" ></i>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <input type="hidden" name="pw_weak" id="pw-weak" class="pw-checkbox" value="on" />
                        <input type="hidden" name="rp_key" value="{{ esc_attr($rp_key)}}">

                        <p class="submit reset-pass-submit text-center">
                            <input type="submit" name="wp-submit" id="wp-submit" class="btn btn-primary mt-3" value="Salva password">
                        </p>
                    </form>
                    
                </div>
                <div class="col-lg-4 col-0"></div>
            </div>

        </div>
    </section>

@endsection

@push("footer")
    <script>
        const togglePassword = document.querySelector('#togglePassword');
        const password = document.querySelector('#password');

        togglePassword.addEventListener('click', function (e) {
            // toggle the type attribute
            const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
            password.setAttribute('type', type);
            // toggle the eye slash icon
            this.classList.toggle('fa-eye-slash');
        });

        const toggleConfirmPassword = document.querySelector('#toggleConfirmPassword');
        const confirmPassword = document.querySelector('#passwordConfirm');

        toggleConfirmPassword.addEventListener('click', function (e) {
            // toggle the type attribute
            const type = confirmPassword.getAttribute('type') === 'password' ? 'text' : 'password';
            confirmPassword.setAttribute('type', type);
            // toggle the eye slash icon
            this.classList.toggle('fa-eye-slash');
        });


        jQuery(function ($) {
            $(document).ready(function () {
                var isLock = false;
                $("form").on('submit', function (e) {
                    if (isLock)
                        return false;

                    isLock = true;

                    $('button[type="submit"]').attr('disabled', true);
                });
            });
        });
    </script>
@endpush