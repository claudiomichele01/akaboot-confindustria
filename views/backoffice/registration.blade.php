@extends('layouts.main')

@section('content')

    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-lg-4 px-lg-5 py-5 gradientBg">
                    <div class="px-lg-5 py-lg-5 text-white">
                        <div class="h3 text-white">
                            Sei un docente della scuola di infanzia o della scuola primaria?
                        </div>
                        <div class="h1 my-5 text-white">Registrati subito, è totalmente gratuito!</div>
                        <div class="pt-lg-5">
                            Inserisci i tuoi dati, aggiungi le tue classi e conferma la registrazione via mail.
                            Poi accedi alla tua area riservata in ogni momento ed entra a far parte del mondo DivertiDenti.
                        </div>
                    </div>
                </div>
                <form class="col-12 col-lg-8 px-lg-5 py-5" method="post" action="{{route('user.registration')}}">
                    <input type="hidden" name="nonce" value={{ wp_create_nonce('sv_api_validation') }}>
                    <div class="row py-lg-5">
                        <div class="col-12 col-lg-6 px-lg-5">
                            <div class="h4 mt-5 mb-3 mt-lg-0">DATI DOCENTE</div>
                            <div class="mb-3">
                                <label for="name" class="small">Scrivi il tuo nome*</label>
                                <input type="text" name="name" class="form-control" id="name" placeholder="" required>
                            </div>
                            <div class="mb-3">
                                <label for="surname" class="small">Scrivi il tuo cognome*</label>
                                <input type="text" name="surname" class="form-control" id="surname" placeholder="" required>
                            </div>
                            <div class="mb-3">
                                <label for="email" class="small">Scrivi l’email con cui vuoi registrarti*</label>
                                <input id="email" type="mail" name="mail" class="form-control" id="email" placeholder="" required>
                                <label for="email" class="small">Questa email ti servirà per accedere alla tua area riservata.</label>
                            </div>
                            <div class="mb-3">
                                <label for="phone" class="small">Scrivi il tuo numero di cellulare*</label>
                                <input type="text" name="phone" class="form-control" id="phone" placeholder="" required>
                            </div>                            
                            <div class="mb-3">
                                <label for="affiliation" class="small">Hai saputo del progetto grazie a*</label>
                                <select class="form-select" id="affiliation" name="affiliation" required>
                                    <option selected disabled>SCEGLI UN'OPZIONE</option>
                                    <option value="una_telefonata_informativa" >UNA TELEFONATA INFORMATIVA</option>
                                    <option value="una_email_informativa">UNA EMAIL INFORMATIVA</option>
                                    <option value="canali_social">CANALI SOCIAL</option>
                                    <option value="un_collega">UN/A COLLEGA</option>
                                    <option value="piattaforma_docentieformazione_it">PIATTAFORMA DOCENTIEFORMAZIONE.IT</option>
                                    <option value="suggerimento_di_un_genitore">SUGGERIMENTO DI UN GENITORE</option>
                                </select>
                            </div>
                            <div class="text-end"><i>*Campi obbligatori</i></div>
                        </div>
                        <div class="col-12 col-lg-6 px-lg-5">
                            <div class="h4 mt-5 mb-3 mt-lg-0">LA TUA SCUOLA</div>
                            <div class="small mb-3">Seleziona la tua scuola (non il nome del Comprensivo, ma quello del plesso).</div>

                            @if($school_grade_accepted->count() > 1)
                                <select id="school-grade" class="form-select mb-3" name="school_grade">
                                    <option selected disabled value>...</option>
                                    @foreach($school_grade_accepted as $sg)
                                        <option value="{{$sg->remote_id}}">{{$sg->name}}</option>
                                    @endforeach
                                </select>
                            @else
                                <input id="school-grade" name="school_grade" type="hidden" value="{{$school_grade_accepted->first()->remote_id}}" >
                            @endif
                            <select id="select-region" class="form-select mb-3" name="region" {{$school_grade_accepted->count() > 1 ? "disabled" : ""}}>
                                <option selected disabled>REGIONE*</option>
                            </select>
                            <select id="select-province" class="form-select mb-3" name="province" aria-label="Default select example" disabled>
                                <option selected disabled>PROVINCIA*</option>
                            </select>
                            <select id="select-city" class="form-select mb-3" name="city" aria-label="Default select example" disabled>
                                <option selected>COMUNE</option>
                            </select>
                            <select id="select-school" class="form-select mb-3" name="school_id" aria-label="Default select example" disabled>
                                <option selected>SELEZIONA LA TUA SCUOLA</option>
                            </select>
                            <div class="small"><i>Non trovi il nome della tua scuola? <span><a href="" data-bs-toggle="modal" data-bs-target="#reportClass">Segnalacelo qui</a></span></i></div>

                            <div id="found_institute_area" class="card border-0 mt-3" style="background-color: #E6EDF8; border-radius: 0.5rem; display: none">
                                <div class="card-body">
                                    <div class="h6 text-primary">
                                        <label id="found_school_name" class=""></label>
                                    </div>
                                    <div class="h6 mb-0 text-primary">
                                        <label id="found_school_address" class=""></label>
                                    </div>
                                </div>
                            </div>

                            {{-- per la primaria --}}
                            <div class="row py-5" id="classes_area" style="display: none">
                                <div class="mb-3 h6">Aggiungi una o più classi</div>

                                @component('components.form-repeater', [ "args" => [
                                      'id' => 'classes'
                                    ]])

                                    @slot('repeater_content')
                                        @include('components.form-row', [ "args" => [
                                          "name" => "eb-1"
                                        ]])
                                    @endslot

                                    @slot('repeater_footer')

                                        <div class="col-12 text-center text-lg-end">
                                            <button type="button" class="btn btn-dark btn-block repeater-addnew cursor-pointer">
                                                <i class="align-middle" data-feather="plus-circle"></i>
                                                Aggiungi
                                            </button>
                                        </div>
                                    @endslot

                                @endcomponent
                            </div>


                            <div class="row pt-5">
                                <div class="form-check ms-2">
                                    <input class="form-check-input" type="checkbox" value="" id="first" required>
                                    <label class="small" for="first">
                                        <i>*Ho letto e accetto il <a href="{!! Boot::childUrl("downloads") !!}/Regolamento_DIVERTIDENTI_22_23.pdf">Regolamento</a></i>
                                    </label>
                                </div>
                                <div class="form-check ms-2">
                                    <input class="form-check-input" name="chk_privacy" type="checkbox" value="" id="second" required>
                                    <label class="small" for="second">
                                        <i>*Ho preso visione dell’Informativa Privacy pubblicata a questo link <a href="{!! Boot::childUrl("downloads") !!}/DivertiDenti_Privacy Policy.pdf">Privacy policy</a></i>
                                    </label>
                                </div>
                                <div class="small pt-3">
                                    <i>
                                        Vorrei restare in contatto con voi e ricevere sul mio indirizzo di posta elettronica ulteriori comunicazioni oltre a quelle per l'iniziativa a cui mi sto registrando, anche di carattere promozionale.
                                    </i>
                                </div>
                                <div class="pl-3">
                                    <input type="radio" id="radio_si" name="consenso_marketing" value="on" checked>
                                    <label for="radio_si">Si</label>
                                    <br />
                                    <input type="radio" id="radio_no" name="consenso_marketing" value="off">
                                    <label for="radio_no">No</label>
                                </div>
                                <div class="col-12 text-center text-lg-start">
                                    <button class="btn btn-primary mt-5" type="submit">Conferma e registrati</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
  
    <!-- Modal -->
    <div class="modal fade" id="reportClass" tabindex="-1" aria-labelledby="reportClassLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="modal-title mb-4 h4 text-center" id="reportClassLabel">Segnalaci la tua scuola</div>
                    <form method="POST" action="{{route("user.report")}}">
                        <div class="mx-4">
                            <div class="mb-3">
                                <label for="not_found_user_name" class="">Nome e/o Cognome*</label>
                                <input id="not_found_user_name" type="text" name="not_found_user_name" class="form-control">
                            </div>
                            <div class="mb-3">
                                <label for="not_found_user_email" class="">La tua email*</label>
                                <input id="not_found_user_email" type="email" name="not_found_user_email" class="form-control">
                            </div>
                            <div class="mb-3">
                                <label for="not_found_school_name" class="">Nome scuola*</label>
                                <input id="not_found_school_name" type="text" name="not_found_school_name" class="form-control">
                            </div>
                            <div class="mb-3">
                                <label for="not_found_school_city" class="">Comune scuola*</label>
                                <input id="not_found_school_city" type="text" name="not_found_school_city" class="form-control">
                            </div>
                            <div class="mb-3">
                                <label for="not_found_school_code" class="">Codice meccanografico</label>
                                <input id="not_found_school_code" type="text" name="not_found_school_code" class="form-control">
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-12 text-center">
                                <button type="submit" class="btn btn-primary">Invia</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @component('components.rules')
	@endcomponent

@endsection

@push("footer")
    <script type="text/javascript">
        jQuery(function ($) {
            $(document).ready(function() {

                @if($school_grade_accepted->count() > 1)
                    $("#school-grade").select2({
                        placeholder: "Seleziona il grado",
                    });

                    $('#school-grade').on("change", function (){
                        $("#classes_area").fadeIn();
                        if ($('#school-grade').val() === "primaria") {
                            $(".class_repeater").each(function (i,e) {
                                $(e).fadeIn();
                            });
                            $(".section_repeater").each(function (i,e) {
                                $(e).fadeIn();
                            });
                            $(".title_repeater").each(function (i,e) {
                                $(e).fadeOut();
                            });
                        }
                        else {
                            $(".class_repeater").each(function (i, e) {
                                $(e).fadeOut();
                            });
                            $(".section_repeater").each(function (i, e) {
                                $(e).fadeOut();
                            });
                            $(".title_repeater").each(function (i, e) {
                                $(e).fadeIn();
                            });
                        }


                        $(this).siblings().children('span.selection').children().removeAttr('style');
                        $('#select-region').prop("disabled", false);
                        if($('#select-region').val() != '' || $('#select-province').val() != '' || $('#select-city').val() != '' ){
                            $('#select-region').val('').trigger('change');
                            $('#select-province').val('').trigger('change');
                            $('#select-city').val('').trigger('change');
                            $('#select-province').prop("disabled", true);
                            $('#select-city').prop("disabled", true);
                            $('#select-school').prop("disabled", true);
                        }
                    });
                @endif

                    $("#select-region").select2({
                        placeholder: "Seleziona la regione",
                        minimumResultsForSearch: Infinity,
                        ajax: {
                            url: '{{ route('select2.region') }}',
                            dataType: 'json',
                            data: function (params) {
                                var query = {
                                    term: params.term,
                                    school_grade: $("#school-grade").val()
                                };
                                return query;
                            },
                            processResults: function (region) {
                                return region;
                            }
                        }
                    });

                $('#select-region').on("change", function (){
                    $(this).siblings().children('span.selection').children().removeAttr('style');
                    $('#select-province').prop("disabled", false);
                    if($('#select-province').val() != '' || $('#select-city').val() != '' ){
                        $('#select-province').val('').trigger('change');
                        $('#select-city').val('').trigger('change');
                        $('#select-city').prop("disabled", true);
                        $('#select-school').prop("disabled", true);
                    }
                });


                $("#select-province").select2({
                    placeholder: "Seleziona la provincia",
                    minimumResultsForSearch: Infinity,
                    ajax: {
                        url: '{{ route('select2.province') }}',
                        data: function (params) {
                            var query = {
                                term: params.term,
                                school_grade: $("#school-grade").val(),
                                region_name: $("#select-region").val()
                            };

                            // Query parameters will be ?search=[term]&type=public
                            return query;
                        },
                        processResults: function (region) {
                            return region;
                        }
                    }
                });

                $('#select-province').on("change", function (){
                    $(this).siblings().children('span.selection').children().removeAttr('style');
                    $('#select-city').prop("disabled", false);
                    if($('#select-city').val() != '' || $('#select-school').val() != '' ){
                        $('#select-city').val('').trigger('change');
                        $('#select-school').val('').trigger('change');
                        $('#select-school').prop("disabled", true);
                    }
                });


                $("#select-city").select2({
                    placeholder: "Seleziona la città",
                    minimumResultsForSearch: Infinity,
                    ajax: {
                        url: '{{ route('select2.city') }}',
                        data: function (params) {
                            var query = {
                                term: params.term,
                                school_grade: $("#school-grade").val(),
                                province_name: $("#select-province").val()
                            };

                            // Query parameters will be ?search=[term]&type=public
                            return query;
                        },
                        processResults: function (region) {
                            return region;
                        }
                    }
                });

                $('#select-city').on("change", function (){
                    $(this).siblings().children('span.selection').children().removeAttr('style');
                    $('#select-school').prop("disabled", false);
                    if($('#select-school').val() != '' ){
                        $('#select-school').val('').trigger('change');
                    }
                });


                $("#select-school").select2({
                    placeholder: "Seleziona l'istituto",
                    minimumResultsForSearch: Infinity,
                    ajax: {
                        url: '{{ route('select2.school') }}',
                        data: function (params) {
                            var query = {
                                term: params.term,
                                school_grade: $("#school-grade").val(),
                                province_name: $("#select-province").val(),
                                city_name: $("#select-city").val(),
                            };

                            return query;
                        },
                        processResults: function (school) {
                            return school;
                        }
                    }
                });

                $('#select-school').on('change', function(e){
                    var institute_remote_id = $(e.target).val();

                    if (institute_remote_id === null) {
                        $("#found_institute_area").fadeOut();
                        return;
                    }

                    $.ajax({
                        url: "{{route("institute_info")}}" + institute_remote_id
                    }).done(function(data) {
                        $("#found_institute_area").fadeIn();
                        $("#found_school_name").text(data.results.name);
                        $("#found_school_address").text(data.results.address);
                    });
                    $(this).siblings().children('span.selection').children().removeAttr('style');
                })


                //Check classi duplicate
                $(document).on('change', 'select[name*=classes]', function() {
                    var $container = $(this).parents('#classes_area');
                    var $couples = $container.find('.aka-repeatable:visible')

                    var couplesArr = $couples.toArray().map( function( couple ) {
                        return $(couple).find('select').toArray().map( function( select ) {
                            return $(select).val();
                        });
                    });

                    var validCouples = couplesArr.filter( function( couple ) {
                        return couple.every( function( piece ) { return piece != null } );
                    }).map( function( couple ) {
                        return couple.join('');
                    });

                    var uniques = validCouples.filter( function(elem, index, arr) {
                        return arr.indexOf(elem) == index
                    });

                    if ( uniques.length !== validCouples.length ) {
                        $(this).val('');
                        var $message = $('<p class="text-danger">La classe indicata è già presente nell\'elenco</p>');
                        $(this).parents('.form-row').parent().append( $message );
                        $message.delay(5000).fadeOut(500, function() {
                            $message.remove();
                        });
                    }
                });


                // -- Validation
                let style = {
                    boxShadow: '1px 1px 10px 1px #dd2222',
                    backgroundImage: "url(\"data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 12 12' width='12' height='12' fill='none' stroke='%23dc3545'%3e%3ccircle cx='6' cy='6' r='4.5'/%3e%3cpath stroke-linejoin='round' d='M5.8 3.6h.4L6 6.5z'/%3e%3ccircle cx='6' cy='8.2' r='.6' fill='%23dc3545' stroke='none'/%3e%3c/svg%3e\")",
                    backgroundRepeat: 'no-repeat',
                    backgroundAttachment: 'scroll',
                    backgroundSize: '16px 18p',
                    backgroundPosition: '98% 50%',
                    cursor: 'auto'
                };

                emailValidation = function( email, e = null ){
                    let regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                    let emailTest = regex.test(email.val());
                    if( emailTest ){
                        email.removeAttr('style');
                        email.siblings().remove();
                    } else if( !regex.test(email.val()) && (email.siblings().length == 0) ){
                        email.css( style );
                        email.after('<p>Il formato dell\'email non è corretto. Es: nome@provider.it</p>');
                    }

                    if( e != null && !emailTest ) {
                        $(window).scrollTop(0);
                        e.preventDefault();
                    }
                };

                privacyValidation = function(checkbox, e) {
                    if( !checkbox[0].checked ){
                        checkbox.css( style )
                        e.preventDefault();
                    } else {
                        checkbox.removeAttr('style');
                    }
                }

                $("#registration_form").on('submit', function(e){
                    let default_fields = $('.validation');
                    let selects2 = $('.select2-selection--single');
                    let checkprivacy = $('#check-privacy');
                    let email = $('#email');

                    selects2.each(function(){
                        if($(this).parents('span').siblings('select').val() == null){
                            $(this).css( style )
                            $(window).scrollTop(0)
                            e.preventDefault();
                        }
                    });

                    default_fields.each(function(){
                        if(($(this).val() == '' || $(this).val() == null) ){
                            $(this).css( style )
                            $(window).scrollTop(0)
                            e.preventDefault();
                        }
                    });

                    privacyValidation( checkprivacy, e );
                    emailValidation( email, e );
                })

                $('#email').on('change', function(){
                    emailValidation($(this));
                });

                $('#check-privacy').on('change', function(e){
                    privacyValidation( $(this), e );
                })

                $('.validation').each(function(){
                    $(this).on('change', function(){
                        if(($(this).val() != '' || $(this).val() != null) && ($(this).attr('id') != 'email')){
                            $(this).removeAttr('style');
                        }
                    });
                });

                

                var isLock = false;
                $("form").on('submit', function (e) {
                    if (isLock)
                        return false;

                    isLock = true;

                    $('button[type="submit"]').attr('disabled', true);
                });
            });
        });
    </script>
@endpush