@extends('layouts.main')

@section('content')

    <section class="position-relative vh-100">
        <div class="container newPasswordComplete">
            <div class="row">

                <div class="col-12 text-center py-5">
                    <p class="h1 my-3 text-center">
                        {!! Boot::acf()->options->confirm_np->title ?? tfb(4) !!}
                    </p>

                    <p class="text-center mx-5">
                        {!! Boot::acf()->options->confirm_np->text ?? tfb(11) !!}
                    </p>
                    <a href="{{route('reserved_area.login')}}" class="btn btn-primary">{!! Boot::acf()->options->confirm_np->button_text ?? tfb(3) !!}</a>
                </div>
                
            </div>
        </div>
    </section>

@endsection
