@extends('layouts.main')

@section('content')

    <section class="position-relative vh-100">
        <div class="container">
            <div class="row mb-5 py-5">

                <div class="col-12 text-center py-5">
                    <img src="{!! Boot::acf()->options->report->image->url ?? ifb() !!}" class="image404" alt="{!! Boot::acf()->options->report->image->alt ?? tfb(1) !!}">
                    <p class="h1 my-3 text-center">
                        {!! Boot::acf()->options->report->title ?? tfb(2) !!}
                    </p>

                    <p class="text-center mx-5">
                        {!! Boot::acf()->options->report->text ?? tfb(21) !!}
                    </p>
                </div>
                
            </div>
        </div>
    </section>

@endsection
