@extends('backoffice.layouts.teacher-class-add-content')

@section('add-class-content')

    @component('components.upperContent', [ "args" => [
        "title" => "Classe " . $className . " - " . Boot::acf()->options->upload->topbar ?? tfb(3),
    ]])
    @endcomponent

    <section class="py-5">
        <div class="container">
            <div class="row mx-lg-5 mx-2">
                <div class="col-12 text-center">
                    <div class="h2">{!! Boot::acf()->options->upload->title ?? tfb(7) !!}</div>
                    <div>
                        {!! Boot::acf()->options->upload->text ?? tfb(54) !!}
                    </div>
                    <div>
                        <i>ATTENZIONE</i>: nelle foto dei contributi non deve mai comparire il cognome del minore.
                    </div>
                </div>
            </div>
            <div>

                @include('react.hook')
                
            </div>
        </div>
    </section>

    @component('components.rules')
    @endcomponent

@endsection