@extends('layouts.main')

@section('content')

    <section class="position-relative">
        <div class="container">

            <div class="row pt-5">
                <div class="col-lg-3 col-0"></div>
                <div class="col-lg-6 col-12 pt-5">
                    <p class="h1 my-3 text-center">
                        {!! Boot::acf()->options->recover_password->title ?? tfb(4) !!}
                    </p>

                    <p class="text-center h5">
                        {!! Boot::acf()->options->recover_password->text ?? tfb(8) !!}
                    </p>
                </div>
                <div class="col-lg-3 col-0"></div>
            </div>

            <div class="row mt-3">
                <div class="col-lg-4 col-0"></div>
                <div class="col-lg-4 col-12 pb-5">
                    <div class="mb-3">

                        <form name="lostpasswordform" id="lostpasswordform" action="{!! home_url() !!}/wp-login.php?action=lostpassword" method="post">
                            <p>
                                <label for="user_login" class="small">Scrivi la tua email</label>
                                <input type="text" class="form-control" placeholder="Email" required name="user_login" id="user_login" class="input" value="" size="20" autocapitalize="none" autocomplete="off">
                                <label for="user_login" class="small">Riceverai un link per creare una nuova password</label>
                            </p>
                                <input type="hidden" name="redirect_to" value="">
                            <p class="submit text-center">
                                <input type="submit" name="wp-submit" id="wp-submit" class="btn btn-primary" value="Inviami il link">
                            </p>
                        </form>
                    </div>
                    
                </div>
                <div class="col-lg-4 col-0"></div>
            </div>

            <div class="row text-center pb-5 mb-5">
                <div class="col-12">
                    <div class="fw-bold" id="footer-text" style="display: none">
                        {!! Boot::acf()->options->recover_password->activation_text ?? tfb(14) !!} 
                    </div>
                </div>
            </div>

        </div>
    </section>

@endsection

@push("footer")
    <script>
        const button = document.querySelector('#wp-submit');
        const footer_text = document.querySelector('#footer-text');

        button.addEventListener('click', function (e) {
            footer_text.style.display = "block";
        });
    </script>
@endpush