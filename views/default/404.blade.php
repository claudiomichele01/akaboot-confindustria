@extends('layouts.main')

@section('content')

    <section class="position-relative">
        <div class="container">
            <div class="row my-5">

                <div class="col-12 text-center">
                    {{-- <img src="{!! Boot::acf()->options->special_pages->immagine_di_sfondo->url ?? ifb() !!}" class="image404" alt=""> --}}
                    <img src="{!! home_url() !!}/wp-content/themes/divertidenti/images/mionins-banana.png" class="image404" alt="">

                    <p class="h1 my-3 text-center">
                        {{-- {!! Boot::acf()->options->special_pages->titolo !!} --}}
                        404
                    </p>

                    <p class="text-center">
                        {{-- {!! Boot::acf()->options->special_pages->sottotitolo ?? tfb(7) !!} --}}
                        Oops, sembra che la pagina che stai cercando non esista
                    </p>

                    <div class="text-center mb-3">
                        {{-- <a href="{!! Boot::acf()->options->special_pages->pulsante->link_pulsante !!}"><button type="button" class="btn btn-lg btn-primary">{!! Boot::acf()->options->special_pages->pulsante->testo_pulsante !!}</button></a> --}}
                        <a href="{!! home_url() !!}" class="btn btn-lg btn-primary">Torna alla home</a>
                    </div>
                </div>
                
            </div>
        </div>
    </section>

@endsection