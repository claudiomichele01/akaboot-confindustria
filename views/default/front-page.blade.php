@php
	$giochi = Boot::query( 'giochi' );
	$scoperta = Boot::query( 'scoperta-dei-denti' );
	
	$posts = array_merge( $giochi, $scoperta);

	$post = shuffle( $posts );

	$carousels = array_chunk($posts, 4, false);
@endphp

@extends('layouts.main')

@section('content')

	<section class="d-none d-lg-block">
		<div class="container">
			<video autoplay muted loop width="100%" height="auto">
				<source src="{!! Boot::acf()->acf->video_section->video->url ?? tfb(1) !!}" type="video/mp4">
			</video>
		</div>
	</section>

	<section class="d-block d-lg-none">
		<div class="container">
			<div class="ratio ratio-1x1">
				<img src="{!! Boot::acf()->acf->image_mobile->url ?? ifb() !!}" class="images" alt="{!! Boot::acf()->acf->image_mobile->url ?? ifb() !!}" style="object-fit: contain">
			</div>
		</div>
	</section>
	
	<section class="mb-5 py-4 gradientBg">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="h3 text-center text-white">
						{!! Boot::acf()->acf->video_section->row_text ?? tfb(20) !!}
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="pt-5 pb-3">
		<div class="container">
			<div class="row mx-lg-5 mx-2">
				<div class="col-12 col-lg-5 my-auto pe-0">
					<div class="h1">{!! Boot::acf()->acf->second_section->title ?? tfb(8) !!}</div>
					{!! Boot::acf()->acf->second_section->text ?? tfb(36) !!}
				</div>

				<div class="col-12 col-lg-7">
					<div class="ratio ratio-4x3">
						<img src="{!! Boot::acf()->acf->second_section->image->url ?? ifb() !!}" class="images p-lg-5" alt="{!! Boot::acf()->acf->second_section->image->title ?? tfb(1) !!}">
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="text-center">
		<img src="{!! Boot::acf()->acf->second_section->arrows->url ?? ifb() !!}" style="object-fit: none;" alt="{!! Boot::acf()->acf->second_section->image->title ?? tfb(1) !!}">
	</section>

	<section class="pb-5">
		<div class="container">
			<div class="row mx-lg-5 mx-2">
				<div id="carouselExampleIndicators" class="carousel carousel-dark slide" data-bs-ride="true" data-bs-touch="true">
					<div class="carousel-indicators">
					  <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
					  <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
					</div>
					<div class="carousel-inner mb-lg-3 mb-5">
						@php
							$array = Boot::acf()->acf->carousel;
						@endphp
						@foreach( Boot::acf()->acf->carousel as $car )
							<div class="carousel-item {{ $car == $array[0] ? " active" : "" }}" data-bs-interval="15000">
								<div class="row">
									<div class="col-12 col-lg-6">
										<div class="ratio ratio-1x1">
											<img src="{!! $car->image->url ?? ifb() !!}" class="images" style="object-fit: contain" alt="{!! $car->image->title ?? tfb(1) !!}">
										</div>
									</div>
									<div class="col-12 col-lg-6 my-auto pe-lg-5">
										<div class="h1">{!! $car->title ?? tfb(8) !!}</div>
										<div class="py-2">
											{!! $car->text ?? tfb(68) !!}
										</div>
									</div>
								</div>
							</div>
						@endforeach
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="py-5">
		<div class="container">
			<div class="row mx-lg-5 mx-2">

				<div class="card border-0 px-0 cardShadow">
					<div class="card-body m-lg-5 mb-lg-3 m-1">
						<div class="row">
							<div class="col-12 col-lg-8">
								<div class="h1">{!! Boot::acf()->acf->card->title ?? tfb(6) !!}</div>
								<div class="py-2">
									{!! Boot::acf()->acf->card->text ?? tfb(72) !!}
								</div>
							</div>
							<div class="col-12 col-lg-4">
								<div class="position-relative ratio ratio-1x1 mt-lg-0 mt-4">
									<img src="{!! Boot::acf()->acf->card->image->url ?? ifb() !!}" class="images" style="object-fit: contain" alt="{!! Boot::acf()->acf->card->image->title ?? tfb(1) !!}">
								</div>
							</div>
						</div>
					</div>
					<div class="card border-0 gradientBg">
						<div class="card-body m-lg-3 m-1">
							<div class="row text-center">
								<div class="col-12">
									<div class="h1 text-white">{!! Boot::acf()->acf->card->second_card->title ?? tfb(6) !!}</div>
									<div class="h5 py-2 text-white mx-lg-5">
										{!! Boot::acf()->acf->card->second_card->text ?? tfb(31) !!}
									</div>
									<a href="{!! Boot::acf()->acf->card->second_card->button->link->url ?? tfb(1) !!}" class="btn btn-light text-primary mb-4">{!! Boot::acf()->acf->card->second_card->button->title ?? tfb(3) !!}</a>
									<p class="h5 text-white mx-lg-5">
										La giuria di qualità ha selezionato le prime scuole vincitrici: <a href="https://www.divertidenti.it/premi/"><b><u>scoprile qui</u></b></a> <br>
										La prossima selezione avverrà dopo il 14 maggio 2023.
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</section>

	<section class="text-center py-5">
		<img src="{!! Boot::acf()->acf->card->arrow->url ?? ifb() !!}" style="object-fit: none;" alt="{!! Boot::acf()->acf->card->arrow->title ?? tfb(1) !!}">
	</section>

	<section class="py-5">
		<div class="container">
			<div class="row mx-lg-5 mx-2">
				<div class="col-12 text-center">
					<div class="h2">{!! Boot::acf()->acf->article_carousel->title ?? tfb(4) !!}</div>
					<div class="mb-3">
						{!! Boot::acf()->acf->article_carousel->text ?? tfb(24) !!}
					</div>
				</div>
			</div>

			{{-- carousel desktop --}}
			<div id="posts" class="carousel d-none d-lg-block carousel-dark slide" data-bs-ride="carousel">
				<div class="carousel-inner px-5">
					@foreach ($carousels as $carousel)
						<div class="carousel-item {{ $carousel == $carousels[0] ? " active" : "" }}" data-bs-interval="10000000">
							<div class="row mb-4">
								@foreach ($carousel as $post)
									@component('components.cards', [ "args" => [
										"layout" => "col-12 col-lg-3 col-md-6 px-4 d-flex",
										"image" => "$post->thumbnail", 
										"uppertitle" => $post->post_type == 'giochi' ? 'GIOCHI PER LAVARE I DENTI' : 'ALLA SCOPERTA DEI DENTI',
										"title" => $post->post_title,
										"text" => $post->post_excerpt,
										"link" => $post->permalink,
										"button_text" => "Vai",
									]])
									@endcomponent
								@endforeach
							</div>
						</div>
					@endforeach
				</div>
				<button class="carousel-control-prev" type="button" style="justify-content: start" data-bs-target="#posts" data-bs-slide="prev">
				  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
				  <span class="visually-hidden">Previous</span>
				</button>
				<button class="carousel-control-next" type="button" style="justify-content: end" data-bs-target="#posts" data-bs-slide="next">
				  <span class="carousel-control-next-icon" aria-hidden="true"></span>
				  <span class="visually-hidden">Next</span>
				</button>
			</div>

			{{-- carousel mobile --}}
			<div id="postsMobile" class="carousel d-block d-lg-none carousel-dark slide" data-bs-ride="carousel" data-bs-interval="10000000">
				<div class="carousel-inner px-5">
					@foreach( $posts as $post )
						<div class="carousel-item {{ $post == $posts[0] ? " active" : "" }}">
							<div class="row mb-4 text-md-center">
								@component('components.cards', [ "args" => [
									"layout" => "col-12 col-lg-3 px-4 d-flex mdCard",
									"image" => $post->thumbnail, 
									"uppertitle" => $post->post_type == 'giochi' ? 'GIOCHI PER LAVARE I DENTI' : 'ALLA SCOPERTA DEI DENTI',
									"title" => $post->post_title,
									"text" => $post->post_excerpt,
									"link" => $post->permalink,
									"button_text" => "Vai",
								]])
								@endcomponent
							</div>
						</div>
					@endforeach
				</div>
				<button class="carousel-control-prev" type="button" style="justify-content: start" data-bs-target="#postsMobile" data-bs-slide="prev">
				  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
				  <span class="visually-hidden">Previous</span>
				</button>
				<button class="carousel-control-next" type="button" style="justify-content: end" data-bs-target="#postsMobile" data-bs-slide="next">
				  <span class="carousel-control-next-icon" aria-hidden="true"></span>
				  <span class="visually-hidden">Next</span>
				</button>
			</div>

			<div class="row text-center">
				<div class="col-12">
					<a href="{!! Boot::acf()->acf->article_carousel->games_button->link->url ?? tfb(1) !!}" class="btn btn-primary mt-5">{!! Boot::acf()->acf->article_carousel->games_button->title ?? tfb(7) !!}</a> <br>
					<a href="{!! Boot::acf()->acf->article_carousel->learn_more_button->link->url ?? tfb(1) !!}" class="btn btn-primary mt-3">{!! Boot::acf()->acf->article_carousel->learn_more_button->title ?? tfb(5) !!}</a>
				</div>
			</div>
		</div>
	</section>

	<section class="py-5 gradientBg">
		<div class="container-fluid">
			<div class="row mx-lg-3 mx-2">

				<div class="col-0 col-lg-3">
				</div>
				<div class="col-12 col-lg-6 text-center">
					<div class="row">
						<div class="col-12">
							<div class="h2 text-white">{!! Boot::acf()->acf->brand_section->title ?? tfb(4) !!}</div>
						</div>
						<div class="col-12 my-3 py-4 px-0 bg-white" style="border-radius: 2rem">
							<img src="{!! Boot::acf()->acf->brand_section->brand_logo->url ?? ifb() !!}" class="w-75 w-lg-100" alt="{!! Boot::acf()->acf->brand_section->brand_logo->title ?? tfb(2) !!}">
						</div>
						<div class="col-12">
							<div class="h5 text-white">
								{!! Boot::acf()->acf->brand_section->text ?? tfb(47) !!}
							</div>
							<a href="{!! Boot::acf()->acf->brand_section->button->link->url ?? tfb(1) !!}" class="btn btn-white text-primary mt-4">{!! Boot::acf()->acf->brand_section->button->title ?? tfb(3) !!}</a>
						</div>
					</div>
				</div>
				<div class="col-0 col-lg-3">

				</div>
			</div>
		</div>
	</section>

@endsection
