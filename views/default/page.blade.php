@extends('layouts.main')

@section('content')

  <section class="container py-4 py-lg-5">
      <div class="row">
          <div class="col-12">
              <h1 class="h2">{!! Boot::acf()->wp->post->post_title ?? tfb(4) !!}</h1>
              {!! Boot::content() !!}
          </div>
      </div>
  </section>

@endsection