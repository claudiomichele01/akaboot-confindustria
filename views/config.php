<?php
//https://developer.wordpress.org/themes/basics/template-hierarchy/

BladeMap::fallback()->to('default.fallback');

BladeMap::from('page')->type('premi')->to('pages.premi');
BladeMap::from('page')->type('gallery')->to('pages.gallery');
BladeMap::from('page')->type('igiene-orale-in-classe')->to('pages.igiene-in-classe');

BladeMap::from('archive')->type('giochi')->to('archives.giochi');
BladeMap::from('archive')->type('scoperta-dei-denti')->to('archives.scoperta');
BladeMap::from('archive')->type('promotori')->to('archives.promotori');

BladeMap::from('single')->type('giochi')->to('single.gioco');
BladeMap::from('single')->type('scoperta-dei-denti')->to('single.scoperta');
BladeMap::from('single')->type('scoperta-dei-denti')->slug('come-si-chiamano')->to('single.come-si-chiamano');
BladeMap::from('single')->type('scoperta-dei-denti')->slug('come-si-lavano')->to('single.come-si-lavano');
BladeMap::from('single')->type('scoperta-dei-denti')->slug('cosa-mangiare')->to('single.cosa-mangiare');
BladeMap::from('single')->type('scoperta-dei-denti')->slug('il-calendario-dei-denti')->to('single.calendario-dei-denti');
BladeMap::from('single')->type('promotori')->to('single.promotore');