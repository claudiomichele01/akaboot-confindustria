@extends('layouts.main')

@section('content')

    @component('components.upperContent', [ "args" => [
        "title" => Boot::acf()->acf->band_title ?? tfb(5),
    ]])
	@endcomponent
	
	<section class="py-5">
        <div class="container container-padding">
            <div class="row mx-lg-5 mx-2">
                <div class="col-12 text-center">
                    <div class="h2">{!! Boot::acf()->acf->first_section->title ?? tfb(4) !!}</div>
                    <div class="py-3">
                        {!! Boot::acf()->acf->first_section->text ?? tfb(64) !!}
                    </div>
                    <div class="position-relative ratio ratio-21x9 mt-lg-0 mt-4">
						<img src="{!! Boot::acf()->acf->first_section->image->url ?? ifb() !!}" class="images" style="object-fit: contain" alt="{!! Boot::acf()->acf->first_section->image->title ?? tfb(1) !!}">
					</div>
                </div>
            </div>
        </div>
    </section>

    <section class="pt-5">
        <div class="container container-padding">
            <div class="row mx-lg-5 mx-2">
                <div class="col-12 gradientBg text-center py-3" style="border-radius: 2rem">
                    <div class="h3 text-white">{!! Boot::acf()->acf->second_section->title ?? tfb(8) !!}</div>
                </div>
            </div>
        </div>
    </section>

    <section class="pb-5 pt-4">
        <div class="container container-padding">
            <div class="row mx-lg-5 mx-2">
                <div class="col-12">
                    <div>
                        {!! Boot::acf()->acf->second_section->text ?? tfb(46) !!}
                    </div>
                    <div class="py-3">
                        {!! Boot::acf()->acf->second_section->second_text ?? tfb(49) !!}
                    </div>
                    <div class="position-relative ratio ratio-21x9 mt-lg-0 mt-4">
						<img src="{!! Boot::acf()->acf->second_section->image->url ?? ifb() !!}" class="images" style="object-fit: contain" alt="{!! Boot::acf()->acf->second_section->image->title ?? tfb(1) !!}">
					</div>
                </div>
            </div>
        </div>
    </section>

    <section class="pb-5">
        <div class="row mx-lg-5 mx-2">
            <div class="col-12 text-center">
                <a href="{!! Boot::acf()->acf->button->url ?? tfb(1) !!}" class="btn btn-outline-primary">{!! Boot::acf()->acf->button->title ?? tfb(2) !!}</a>
            </div>
        </div>
    </section>

@endsection
