@extends('layouts.main')

@section('content')

    @component('components.upperContent', [ "args" => [
        "title" => Boot::acf()->acf->band_title ?? tfb(5),
    ]])
	@endcomponent
	
	<section class="py-5">
        <div class="container container-padding">
            <div class="row mx-lg-5 mx-2">
                <div class="col-12 text-center">
                    <div class="h2">{!! Boot::acf()->acf->first_section->title ?? tfb(7) !!}</div>
                    <div class="py-3">
                        {!! Boot::acf()->acf->first_section->text ?? tfb(26) !!}
                    </div>
                </div>
            </div>
            <div class="row mx-lg-5 mx-2 mt-n5">
                @foreach( Boot::acf()->acf->first_section->steps as $ste )
                    <div class="col-lg-4 col-6 text-center mb-4 mb-lg-0">
                        <div class="position-relative ratio ratio-1x1">
                            <img src="{!! $ste->image->url ?? ifb() !!}" class="images p-4 pb-0" style="object-fit: contain" alt="{!! $ste->image->title ?? tfb(1) !!}">
                        </div>
                        <div class="px-2 mt-n4">{!! $ste->text ?? tfb(22) !!}</div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

    <section class="pt-5">
        <div class="container container-padding">
            <div class="row mx-lg-5 mx-2">
                <div class="col-12 gradientBg text-center py-3" style="border-radius: 2rem">
                    <div class="h3 text-white mb-0">{!! Boot::acf()->acf->second_section->title ?? tfb(8) !!}</div>
                </div>
            </div>
        </div>
    </section>

    <section class="pb-5 pt-4">
        <div class="container container-padding">
            <div class="row mx-lg-5 mx-2">
                <div class="col-12">
                    <div class="">
                        {!! Boot::acf()->acf->second_section->text ?? tfb(58) !!}
                    </div>
                </div>
            </div>
            <div class="row pt-5 mx-lg-5 mx-2">
                <div class="col-12 col-lg-6">
                    <div class="col-12 pe-5">
                        <div class="h3">
                            {!! Boot::acf()->acf->second_section->first_column->first_section->title ?? tfb(3) !!}
                        </div>
                        <div>
                            {!! Boot::acf()->acf->second_section->first_column->first_section->text ?? tfb(46) !!}
                        </div>
                    </div>
                    <div class="col-12 mt-5 pe-5">
                        <div class="h3">
                            {!! Boot::acf()->acf->second_section->first_column->second_section->title ?? tfb(6) !!}
                        </div>
                        <div>
                            {!! Boot::acf()->acf->second_section->first_column->second_section->text ?? tfb(38) !!}
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-6 mt-4 mt-lg-0">
                    <div class="position-relative ratio ratio-1x1">
						<img src="{!! Boot::acf()->acf->second_section->second_column->image->url ?? ifb() !!}" class="images" style="object-fit: contain" alt="{!! Boot::acf()->acf->second_section->second_column->image->title ?? tfb(1) !!}">
					</div>
                </div>
            </div>
            <div class="row pt-5 mx-lg-5 mx-2">
                <div class="col-12 col-lg-6 pe-5">
                    <div class="h3">
                        {!! Boot::acf()->acf->second_section->first_column->third_section->title ?? tfb(7) !!}
                    </div>
                    <div>
                        {!! Boot::acf()->acf->second_section->first_column->third_section->text ?? tfb(39) !!}
                    </div>
                </div>
                <div class="col-12 col-lg-6 pe-lg-5 mt-4 mt-lg-0">
                    <div class="h3">
                        {!! Boot::acf()->acf->second_section->second_column->title ?? tfb(3) !!}
                    </div>
                    <div>
                        {!! Boot::acf()->acf->second_section->second_column->text ?? tfb(51) !!}
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="pb-5">
        <div class="row mx-lg-5 mx-2">
            <div class="col-12 text-center">
                <a href="{!! Boot::acf()->acf->button->url ?? tfb(1) !!}" class="btn btn-outline-primary">{!! Boot::acf()->acf->button->title ?? tfb(2) !!}</a>
            </div>
        </div>
    </section>

@endsection
