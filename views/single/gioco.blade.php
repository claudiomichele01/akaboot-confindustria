@extends('layouts.main')

@section('content')

    @component('components.upperContent', [ "args" => [
		"title" => Boot::acf()->acf->band_title ?? tfb(7),
	]])
	@endcomponent

    <section class="py-5">
        <div class="container pt-5">
            <div class="row mx-lg-5 mx-2">
                <div class="card border-0 cardShadow">
                    <div class="card-body m-lg-3">
                        <div class="row">
                            <div class="col-12 col-lg-6 order-lg-1 order-2 my-auto ps-lg-5">
                                <div class="h1 pe-2">{!! Boot::acf()->acf->first_card->title ?? tfb(4) !!}</div>
                                <div class="py-4 pe-lg-5">
                                    {!! Boot::acf()->acf->first_card->text ?? tfb(38) !!}
                                </div>
                                <div class="text-center text-lg-start">
                                    <a target="_blank" href="{!! Boot::childUrl("downloads") !!}/{!! Boot::acf()->acf->first_card->button->file ?? tfb(6) !!}" class="btn btn-primary">{!! Boot::acf()->acf->first_card->button->title ?? tfb(1) !!}</a>
                                </div>
                            </div>
                            <div class="col-12 col-lg-6 order-lg-2 order-1">
                                <div class="position-relative ratio ratio-4x3 mb-4 mb-lg-0">
                                    <img src="{!! Boot::acf()->acf->first_card->image ?? ifb() !!}" class="images" style="border-radius: 1.5rem; object-fit: contain" alt="{!! Boot::acf()->acf->first_card->image ?? ifb() !!}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="py-5">
        <div class="container">
            <div class="row mx-lg-5 mx-2">
                <div class="card border-0 cardShadow">
                    <div class="card-body m-lg-3">
                        <div class="row">
                            <div class="col-12 col-lg-6">
                                <div class="position-relative ratio ratio-4x3 mb-4 mb-lg-0">
                                    <img src="{!! Boot::acf()->acf->second_card->image ?? ifb() !!}" class="images" style="border-radius: 1.5rem; object-fit: contain" alt="{!! Boot::acf()->acf->second_card->image ?? ifb() !!}">
                                </div>
                            </div>
                            <div class="col-12 col-lg-6 my-auto ps-lg-5">
                                <div class="h1">{!! Boot::acf()->acf->second_card->title ?? tfb(8) !!}</div>
                                <div class="py-4">
                                    {!! Boot::acf()->acf->second_card->text ?? tfb(42) !!}
                                </div>
                                <div class="text-center text-lg-start">
                                    <a target="_blank" href="{!! Boot::childUrl("downloads") !!}/{!! Boot::acf()->acf->second_card->button->file ?? tfb(3) !!}" class="btn btn-primary">{!! Boot::acf()->acf->second_card->button->title ?? tfb(1) !!}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="py-5">
        <div class="row mx-lg-5 mx-2">
            <div class="col-12 text-center">
                <a href="{!! Boot::acf()->acf->button->url ?? tfb(1) !!}" class="btn btn-outline-primary">{!! Boot::acf()->acf->button->title ?? tfb(2) !!}</a>
            </div>
        </div>
    </section>

@endsection
