@extends('layouts.main')

@section('content')

	<section>
		<div class="container-fluid mx-0">
            <div class="row px-0">
                <div class="col-12 px-0">
                    <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="true">
                        <div class="carousel-indicators">
                            @php
                                $array = Boot::acf()->acf->carousel;
                                $i = 0;
                            @endphp
                          
                            @foreach( Boot::acf()->acf->carousel as $car )
                                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="{{$i}}" class="active" aria-current="true"></button>
                                @php
                                    $i++;
                                @endphp
                            @endforeach
                        </div>
                        
                        <div class="carousel-inner">
                            @foreach( Boot::acf()->acf->carousel as $car )
                                <div class="carousel-item {{ $car == $array[0] ? " active" : "" }} ratio ratio-3x1 py-lg-0 py-5" data-bs-interval="1000">
                                    <img src="{!! $car->image->url ?? ifb() !!}" class="images" style="object-fit: cover" alt="">
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</section>

	<section class="pt-5" style="background-color: {!! Boot::acf()->acf->section->color ?? tfb(1) !!}">
		<div class="container">
			<div class="row mx-lg-5 mx-2">

                <div class="col-12 text-center">
                    <div class="h1 text-{!! Boot::acf()->acf->section->text_color ?? tfb(1) !!}">{!! Boot::acf()->acf->section->title ?? tfb(3) !!}</div>
                    <div class="text-{!! Boot::acf()->acf->section->text_color ?? tfb(1) !!} py-3">
                        {!! Boot::acf()->acf->section->text ?? tfb(79) !!}
                    </div>
                </div>
			</div>
		</div>
	</section>

    <section class="pb-5" style="background-color: {!! Boot::acf()->acf->section->color ?? tfb(1) !!}">
        <div class="container">
            <div class="row mx-lg-5 mx-1">
                <div class="col-12 pt-5">
                    <div class="card border-0">
                        <div class="card-body mx-lg-5 my-lg-3 m-1">
                            <div class="row">
                                <div class="col-12 col-lg-6 order-lg-1 order-2 pe-lg-5 my-auto">
                                    <div class="h1">{!! Boot::acf()->acf->section->card->title ?? tfb(6) !!}</div>
                                    <div class="">
                                        {!! Boot::acf()->acf->section->card->text ?? tfb(85) !!}
                                    </div>
                                    {{-- <div class="text-center text-lg-start mt-5">
                                        
                                        @foreach( Boot::acf()->acf->section->card->social as $soc )
                                            <a target="_blank" href="{!! $soc->link->url ?? tfb(1) !!}">
                                                <i class="{!! $soc->icon ?? tfb(2) !!} text-primary" style="width: 2rem"></i>
                                            </a>
                                        @endforeach
                                    </div> --}}
                                    
                                    <div class="row mt-5 text-center text-lg-start">
                                        <div class="col-lg-7 col-12">
                                            <a target="_blank" href="{!! Boot::acf()->acf->section->card->button->url ?? tfb(1) !!}" class="btn btn-primary">{!! Boot::acf()->acf->section->card->button->title ?? tfb(3) !!}</a>
                                        </div>
                                        <div class="col-lg-5 col-12 mt-lg-0 mt-4">
                                            @foreach( Boot::acf()->acf->section->card->social as $soc )
                                                <a target="_blank" href="{!! $soc->link->url ?? tfb(1) !!}">
                                                    <i class="{!! $soc->icon ?? tfb(2) !!} text-primary h1 me-4"></i>
                                                </a>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6 order-lg-2 order-1 mb-5 mb-lg-0">
                                    <div class="position-relative ratio ratio-1x1">
                                        <img src="{!! Boot::acf()->acf->section->card->image->url ?? ifb() !!}" class="images p-lg-4 p-0" style="object-fit: contain" alt="{!! Boot::acf()->acf->section->card->image->title ?? tfb(1) !!}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mx-lg-5 mx-2 my-5">
                <div class="col-12 text-center">
                    <a href="{!! Boot::acf()->acf->button->url ?? tfb(1) !!}" class="btn btn-white text-primary">{!! Boot::acf()->acf->button->title ?? tfb(2) !!}</a>
                </div>
            </div>
        </div>
    </section>

@endsection
