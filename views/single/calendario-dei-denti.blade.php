@extends('layouts.main')

@section('content')

    @component('components.upperContent', [ "args" => [
        "title" => Boot::acf()->acf->band_title ?? tfb(4),
    ]])
	@endcomponent
	
	<section class="py-5">
		<div class="container container-padding">
			<div class="row mx-lg-5 mx-2">

				<div class="col-12 col-lg-6 my-auto">
					<div class="h2">{!! Boot::acf()->acf->first_section->title ?? tfb(4) !!}</div>
					<div class="py-2">
                        {!! Boot::acf()->acf->first_section->first_text ?? tfb(44) !!}
                    </div>
					<div>
						{!! Boot::acf()->acf->first_section->second_text ?? tfb(43) !!}
					</div>
				</div>

				<div class="col-12 col-lg-6 px-2 px-lg-0">
					<div class="position-relative ratio ratio-1x1">
						<img src="{!! Boot::acf()->acf->first_section->image->url ?? ifb() !!}" class="images" style="object-fit: contain" alt="{!! Boot::acf()->acf->first_section->image->title ?? tfb(1) !!}">
					</div>
				</div>
			</div>
		</div>
	</section>

    <section class="py-5">
        <div class="container container-padding">
            <div class="row mx-lg-5 mx-2">
                <div class="col-12">
                    <div class="h3">
                        {!! Boot::acf()->acf->first_list->title ?? tfb(6) !!}
                    </div>
                    <div>
                        {!! Boot::acf()->acf->first_list->text ?? tfb(30) !!}
                    </div>
                </div>
            </div>
            <div class="row mx-lg-5 mx-2">
                <div class="col-12 col-lg-6 pe-lg-5">
                    <div class="py-lg-3">
                        <ul class="check mb-4 mb-lg-4">
                            @foreach( Boot::acf()->acf->first_list->first_column->element as $ele )
                                <li class="mb-3 check">
                                    <strong class="calendar">{!! $ele->title ?? tfb(2) !!}</strong> <br>
                                    {!! $ele->text ?? tfb(18) !!}
                                </li>
                            @endforeach
                        </ul>  
                    </div>
                </div>
                <div class="col-12 col-lg-6 pe-lg-5">
                    <div class="py-lg-3">
                        <ul class="check">
                            @foreach( Boot::acf()->acf->first_list->second_column->element as $ele )
                                <li class="mb-3 check">
                                    <strong class="calendar">{!! $ele->title ?? tfb(2) !!}</strong> <br>
                                    {!! $ele->text ?? tfb(18) !!}
                                </li>
                            @endforeach
                        </ul>
                        <div class="position-relative ratio ratio-4x3">
                            <img src="{!! Boot::acf()->acf->first_list->second_column->image ?? ifb() !!}" class="images" style="object-fit: contain" alt="calendario-jordan">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="py-5">
        <div class="container container-padding">
            <div class="row mx-lg-5 mx-2">
                <div class="col-12">
                    <div class="h3">
                        {!! Boot::acf()->acf->second_list->title ?? tfb(6) !!}
                    </div>
                    <div>
                        {!! Boot::acf()->acf->second_list->text ?? tfb(26) !!}
                    </div>
                </div>
            </div>
            <div class="row mx-lg-5 mx-2">
                <div class="col-12 col-lg-6">
                    <div class="py-lg-3">
                        <ul class="check mb-4 mb-lg-0">
                            @foreach( Boot::acf()->acf->second_list->first_column->element as $ele )
                                <li class="mb-3 check">
                                    <strong class="calendar">{!! $ele->title ?? tfb(2) !!}</strong> <br>
                                    {!! $ele->text ?? tfb(36) !!}
                                </li>
                            @endforeach
                        </ul>  
                    </div>
                </div>
                <div class="col-12 col-lg-6">
                    <div class="py-lg-3">
                        <ul class="check">
                            @foreach( Boot::acf()->acf->second_list->second_column->element as $ele )
                                <li class="mb-3 check">
                                    <strong class="calendar">{!! $ele->title ?? tfb(2) !!}</strong> <br>
                                    {!! $ele->text ?? tfb(36) !!}
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="pt-5">
        <div class="container container-padding">
            <div class="row mx-lg-5 mx-2">
                <div class="col-12 gradientBg text-center py-3" style="border-radius: 2rem">
                    <div class="h3 text-white">{!! Boot::acf()->acf->fourth_section->title ?? tfb(8) !!}</div>
                </div>
            </div>
        </div>
    </section>

    <section class="pb-5 pt-4">
        <div class="container container-padding">
            <div class="row mx-lg-5 mx-2">
                <div class="col-12">
                    <div class="">
                        {!! Boot::acf()->acf->fourth_section->first_text ?? tfb(85) !!}
                    </div>
                    <div class="py-3">
                        {!! Boot::acf()->acf->fourth_section->second_text ?? tfb(30) !!}
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="pb-5">
        <div class="row mx-lg-5 mx-2">
            <div class="col-12 text-center">
                <a href="{!! Boot::acf()->acf->button->url ?? tfb(1) !!}" class="btn btn-outline-primary">{!! Boot::acf()->acf->button->title ?? tfb(2) !!}</a>
            </div>
        </div>
    </section>

@endsection
