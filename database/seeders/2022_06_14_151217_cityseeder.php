<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class Cityseeder {
    /**
     * Run the seeder.
     *
     * @return void
     */
    
    public function run(){
        $cities = [
            'Città iper Energetica', 
            'Città in Movimento', 
            'Città dei Cestini', 
            'Città di Cemento', 
            'Città Ideale'
        ];
        
        foreach( $cities as $city ){
            Capsule::table('cities')->insert([
                'name' => $city
            ]);
        }
    }
}