<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class AddColorIntoFilesTable {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Capsule::schema()->table("add_color_into_files_table", function ($table) {
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Capsule::schema()->dropIfExists('add_color_into_files_table');
    }
}