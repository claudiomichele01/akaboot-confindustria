<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class CollectionTable {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        // Capsule::schema()->create("sv_files", function ($table) {
        //     $table->id();
        //     $table->bigInteger('class_id')->unsigned();
        //     $table->bigInteger('city_id')->unsigned();
        //     $table->string('file_content');
        //     $table->timestamps();

        //     $table->foreign('city_id')->references('id')->on('sv_cities');
        //     $table->foreign('class_id')->references('id')->on('sv_classes');
        //     $table->boolean('is_active')->default( false );
        //     $table->enum('status', ['pending', 'not_approved', 'approved'])->default('pending');
        //     $table->string('title')->nullable();
        //     $table->string('description')->nullable();
        //     $table->string('path_dest');
        //     $table->string('color')->nullable();
        // });
        
        Capsule::schema()->create("collection", function ($table) {
            $table->id();
            $table->string("name")->default('gallery');
            $table->bigInteger("max_load")->default('25');
        });

        Capsule::schema()->table("sv_files", function ($table) {
            $table->foreignId("collection_id");
            $table->foreign('collection_id')->references('id')->on('collection');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Capsule::schema()->dropIfExists('collection');

        Capsule::schema()->table("sv_files", function ($table) {
            $table->dropIfExists("collection_id");
        });
    }
}